package com.radiantapp;

import java.util.HashMap;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class ContactusActivity extends Activity {
	double latitude = 21.1645104;
	double longitude = 72.7713561;
	private GoogleMap googleMap;
	private MapFragment mMapFragment;
	private TextView txtaddress, lblBack;
	private SessionManager sessionManager;
	private Context context = this;
	private HashMap<String, String> userDetails;
	private String AppTitle = "Contact Us";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		//this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_contactus);
		// getActionBar().hide();
		sessionManager = new SessionManager(context);
		userDetails = new HashMap<String, String>();
		userDetails = sessionManager.getUserDetails();

		try {
			latitude = Double.parseDouble(userDetails
					.get(SessionManager.KEY_LATTITUDE));
			longitude = Double.parseDouble(userDetails
					.get(SessionManager.KEY_LONGTITUDE));
		} catch (NumberFormatException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}

		try {
			txtaddress = (TextView) findViewById(R.id.txtaddress);
			lblBack = (TextView) findViewById(R.id.lblBack);

			String aa = userDetails.get(SessionManager.KEY_ADDRESS);
			aa = aa.replace(",,", "\n");

			txtaddress.setText("" + aa);

			lblBack.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					Intent i = new Intent(ContactusActivity.this,
							MainActivity.class);
					i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					startActivity(i);

				}
			});
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		try {

			// Loading map
			initilizeMap();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void initilizeMap() {
		try {

			// Changing marker icon
			// marker.icon(BitmapDescriptorFactory.fromResource(R.drawable.my_marker_icon)));

			// adding marker
			if (googleMap == null) {
				googleMap = ((MapFragment) getFragmentManager()
						.findFragmentById(R.id.map)).getMap();

				// Projection projection = googleMap.getProjection();
				// create marker
				MarkerOptions marker = new MarkerOptions().position(
						new LatLng(latitude, longitude)).title(
						"Radiant English Academy");

				// Changing marker icon
				marker.icon(BitmapDescriptorFactory
						.defaultMarker(BitmapDescriptorFactory.HUE_ROSE));
				// adding marker

				LatLng latLng = new LatLng(latitude, longitude);
				CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(
						latLng, 15);
				googleMap.animateCamera(cameraUpdate);
				// locationManager.removeUpdates(this);

				googleMap.addMarker(marker);
				googleMap.setMyLocationEnabled(true);
				// check if map is created successfully or not
				if (googleMap == null) {
					Toast.makeText(getApplicationContext(),
							"Sorry! unable to create maps", Toast.LENGTH_SHORT)
							.show();
				}

			}
		} catch (Exception e) {
			System.out.print("Error :" + e.getMessage());
		}

	}
}
