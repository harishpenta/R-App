package com.radiantapp;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.radiantapp.FragmentRemark.GetRemarkDetailsFromServer;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SimpleAdapter;
import android.widget.TextView;

public class LeaveDetailsActivity extends Activity {

	private SessionManager sessionmanager;
	private HashMap<String, String> userDetails;
	String url;
	private ArrayList<HashMap<String, String>> Leavelist = new ArrayList<HashMap<String, String>>();
	private ListView listview;
	private JSONArray jarrayremark;
	private String fromdt, todt, type, desc, status;
	private dbhandler db;
	private SQLiteDatabase sd;
	private String emptid;
	private TextView lblBackLeave, lblSyncTextLeave, lblApproval;
	ProgressBar pgrShowLeave;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_leave_details);

		sessionmanager = new SessionManager(getApplicationContext());
		userDetails = new HashMap<String, String>();
		userDetails = sessionmanager.getUserDetails();
		db = new dbhandler(getApplicationContext());
		sd = db.getReadableDatabase();
		sd = db.getWritableDatabase();
		emptid = userDetails.get(sessionmanager.KEY_EMPID);
		lblBackLeave = (TextView) findViewById(R.id.lblBackLeave);
		listview = (ListView) findViewById(R.id.lstViewLeave);
		pgrShowLeave = (ProgressBar) findViewById(R.id.pgrShowLeave);
		lblSyncTextLeave = (TextView) findViewById(R.id.lblSyncTextLeave);
		lblApproval = (TextView) findViewById(R.id.lblApproval);
		lblBackLeave.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent i = new Intent(LeaveDetailsActivity.this,
						MainActivity.class);
				i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(i);

			}
		});

		if (NetConnectivity.isOnline(getApplicationContext())) {
			new GetRemarkDetailsFromServer().execute("");
		} else {
			FillDataonListView();
		}

	}

	class GetRemarkDetailsFromServer extends AsyncTask<String, Void, String> {

		private String url1;
		private ProgressDialog pDialog;

		@Override
		protected void onPreExecute() {
			pgrShowLeave.setVisibility(View.VISIBLE);
			lblSyncTextLeave.setVisibility(View.VISIBLE);
			listview.setVisibility(View.GONE);
			super.onPreExecute();
		}

		@Override
		protected String doInBackground(String... urls) {

			String url = AllKeys.WEBSITE
					+ "GetJSONForLeaveDetailsByStudents?type=LeaveDetails&clientid="
					+ AllKeys.CLIENT_ID + "&branch="
					+ userDetails.get(SessionManager.KEY_BRANCHID) + "&empid="
					+ userDetails.get(SessionManager.KEY_EMPID);
			Log.d("leaves", url);
			ServiceHandler sh = new ServiceHandler();
			String str_stud_leave = sh.makeServiceCall(url, ServiceHandler.GET);

			if (str_stud_leave != null && !str_stud_leave.equals("")) {

				str_stud_leave = "{" + '"' + "Leave" + '"' + ":"
						+ str_stud_leave + "}";

				try {
					JSONObject json = new JSONObject(str_stud_leave);

					if (json != null) {
						try {
							Log.d("Leaves Json data", json.toString());
							// Getting Array of Contacts
							jarrayremark = json.getJSONArray("Leave");

							// looping through All Contacts
							sd.delete("leaves", null, null);
							for (int i = 0; i < jarrayremark.length(); i++) {
								JSONObject c = jarrayremark.getJSONObject(i);

								sd.execSQL("INSERT INTO leaves VALUES(null,'"
										+ c.getString("FromDt") + "','"
										+ c.getString("ToDt") + "','"
										+ c.getString("Type") + "','"
										+ c.getString("Description") + "','"
										+ c.getString("Status") + "') ");
							}

						} catch (JSONException e) {
							e.printStackTrace();
							Log.d("Leaves json error", e.getMessage());
						}
					}
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}

			return null;
		}

		@SuppressLint("SimpleDateFormat")
		@Override
		protected void onPostExecute(String result) {
			pgrShowLeave.setVisibility(View.GONE);
			lblSyncTextLeave.setVisibility(View.GONE);
			listview.setVisibility(View.VISIBLE);

			FillDataonListView();

		}

	}

	public void FillDataonListView() {

		try {
			Cursor c = sd.rawQuery("select * from leaves", null);
			Leavelist.clear();
			while (c.moveToNext()) {

				fromdt = c.getString(1);
				todt = c.getString(2);
				type = c.getString(3);
				desc = c.getString(4);
				status = c.getString(5);
				String approval;
				if (status == "1" || status.equals("1")) {
					approval = "Approved";
				} else {
					approval = "Not Approved";
				}
				HashMap<String, String> map = new HashMap<String, String>();

				map.put("FROM", fromdt);
				map.put("TO", todt);
				map.put("TYPE", type);
				map.put("DESC", desc);
				map.put("APPROVE", approval);

				Leavelist.add(map);
			}

			SimpleAdapter adapter = new SimpleAdapter(getApplicationContext(),
					Leavelist, R.layout.list_leave_items, new String[] {
							"FROM", "TO", "TYPE", "DESC", "APPROVE" },
					new int[] { R.id.lblFrom, R.id.lblTo, R.id.lblType,
							R.id.lblDesc, R.id.lblApproval });

			listview.setAdapter(adapter);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.leave_details, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
