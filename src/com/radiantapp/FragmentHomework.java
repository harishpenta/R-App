package com.radiantapp;

import java.io.BufferedInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.radiantapp.FilesInDownloadsActivity.DownloadFileFromURL;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

public class FragmentHomework extends Activity {

	ArrayList<HomeWork> homeworklista = new ArrayList<HomeWork>();
	ArrayList<HashMap<String, String>> homeworklist = new ArrayList<HashMap<String, String>>();
	Activity a;

	Button btnView, btnDownload;
	private String deptid;
	private SessionManager sessionmanager;
	private HashMap<String, String> userDetails;
	private ListView listview;
	private Cursor c;
	private String urls;
	private String url;
	private JSONArray jarrayhomework;
	private String homeworkname, getId, facultyname, date, sub, homeid,
			hometype, imagename;
	private dbhandler db;
	private SQLiteDatabase sd;
	ProgressBar pgrShowHomework;
	TextView lblShowTextSync, lblBackHomework;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_home_work);

		try {
			sessionmanager = new SessionManager(FragmentHomework.this);
			userDetails = new HashMap<String, String>();

			userDetails = sessionmanager.getUserDetails();
			db = new dbhandler(FragmentHomework.this);
			sd = db.getReadableDatabase();
			sd = db.getWritableDatabase();

			deptid = userDetails.get(sessionmanager.KEY_DEPTID);
			lblBackHomework = (TextView) findViewById(R.id.lblBackHomework);
			listview = (ListView) findViewById(R.id.lstView);
			pgrShowHomework = (ProgressBar) findViewById(R.id.pgrShowHomework);
			lblShowTextSync = (TextView) findViewById(R.id.lblSyncTextHomework);

			if (NetConnectivity.isOnline(FragmentHomework.this)) {
				new GetHomewrokDetailsFromServer().execute();
			} else {
				FillDataonListView();
			}

			lblBackHomework.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					Intent i = new Intent(FragmentHomework.this,
							MainActivity.class);
					i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					startActivity(i);

				}
			});

			listview.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> parent, View v,
						int position, long arg3) {

					try {
						getId = ((TextView) v.findViewById(R.id.txthomeid))
								.getText().toString();
						db = new dbhandler(FragmentHomework.this);
						sd = db.getReadableDatabase();
						Cursor cr = sd.rawQuery(
								"select * from homework where id=" + "'"
										+ getId + "'", null);
						while (cr.moveToNext()) {
							hometype = cr.getString(4);
							imagename = cr.getString(5);
						}

						if (hometype.equals("Text")) {

						} else if (hometype.equals("Image")) {

							showDialogBox();

						}
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}
			});
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void showDialogBox() {
		try {

			// ***************************

			final Dialog dialog = new Dialog(FragmentHomework.this);

			// setting custom layout to dialog
			dialog.setContentView(R.layout.prompt_dialog);
			dialog.setTitle("Select Option");

			// adding text dynamically
			btnView = (Button) dialog.findViewById(R.id.btnViewOnly);
			btnDownload = (Button) dialog.findViewById(R.id.btnDownloadOnly);

			// ***************************
			// dialog.setCancelable(false);
			dialog.show();

			btnView.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View view) {

					try {
						db = new dbhandler(FragmentHomework.this);
						sd = db.getReadableDatabase();
						Cursor cr = sd.rawQuery(
								"select * from homework where id=" + "'"
										+ getId + "'", null);
						while (cr.moveToNext()) {
							hometype = cr.getString(4);
							imagename = cr.getString(5);
						}

						Intent ik = new Intent(FragmentHomework.this,
								HomeworkImageDisplay.class);
						ik.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
						ik.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
						ik.putExtra("IMAGELINK", imagename);
						startActivity(ik);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}
			});

			btnDownload.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {

					try {
						dialog.dismiss();

						Cursor cr = sd.rawQuery(
								"select * from homework where id=" + "'"
										+ getId + "'", null);
						while (cr.moveToNext()) {
							hometype = cr.getString(4);
							imagename = cr.getString(5);
						}
						new DownloadFileFromURL().execute(imagename);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			});
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void FillDataonListView() {

		db = new dbhandler(FragmentHomework.this);
		sd = db.getReadableDatabase();

		Cursor c = sd.rawQuery("select * from homework", null);

		homeworklist.clear();
		while (c.moveToNext()) {

			homeid = c.getString(0);
			homeworkname = c.getString(1);
			facultyname = c.getString(2);
			date = c.getString(3);
			sub = c.getString(6);

			HashMap<String, String> map = new HashMap<String, String>();

			map.put("ID", homeid);
			map.put("HOMEWORK", homeworkname);
			map.put("FACULTY", "Teacher : " + facultyname);
			map.put("DATE", date);
			map.put("SUBJECT", sub);

			homeworklist.add(map);
		}

		SimpleAdapter adapter = new SimpleAdapter(FragmentHomework.this,
				homeworklist, R.layout.lst_row_single_homework, new String[] {
						"ID", "HOMEWORK", "FACULTY", "DATE", "SUBJECT" },
				new int[] { R.id.txthomeid, R.id.lblHomeWOrk, R.id.lblFaculty,
						R.id.lblDate, R.id.lblSubject });

		listview.setAdapter(adapter);

	}

	// ***************************************

	class GetHomewrokDetailsFromServer extends AsyncTask<String, Void, String> {

		private String url1;
		private ProgressDialog pDialog;

		@Override
		protected void onPreExecute() {
			// pDialog = new ProgressDialog(FragmentHomework.this);
			// pDialog.setMessage("Please wait...");
			// pDialog.show();
			// pDialog.setCancelable(false);

			pgrShowHomework.setVisibility(View.VISIBLE);
			lblShowTextSync.setVisibility(View.VISIBLE);
			listview.setVisibility(View.GONE);
			super.onPreExecute();
		}

		@Override
		protected String doInBackground(String... urls) {

			JSONParser jParser = new JSONParser();

			// getting JSON string from URL=
			// url = AllKeys.WEBSITE + "json_data.aspx?DeptId=" + deptid
			// + "&type=homework";

			url = AllKeys.WEBSITE
					+ "GetHomeworkDetailData?type=homework&deptid=" + deptid
					+ "&clientid=" + AllKeys.CLIENT_ID + "&branch="
					+ userDetails.get(SessionManager.KEY_BRANCHID);
			Log.d("url", url);
			// url = AllKeys.WEBSITE+ "json_data.aspx?DeptId=" + "3" +
			// "&type=homework";
			// JSONObject json = jParser.getJSONFromUrl(url);
			ServiceHandler sh = new ServiceHandler();
			String str_stud_remark = sh
					.makeServiceCall(url, ServiceHandler.GET);
			ContentValues cv = new ContentValues();
			if (str_stud_remark != null && !str_stud_remark.equals("")) {
				str_stud_remark = "{" + '"' + "HomeworkData" + '"' + ":"
						+ str_stud_remark + "}";
				try {
					JSONObject json = new JSONObject(str_stud_remark);
					if (json != null) {

						try {
							Log.d("Homework Json data", json.toString());
							// Getting Array of Contacts
							jarrayhomework = json.getJSONArray("HomeworkData");

							// looping through All Contacts
							sd.delete("homework", null, null);
							for (int i = 0; i < jarrayhomework.length(); i++) {
								JSONObject c = jarrayhomework.getJSONObject(i);

								sd.execSQL("INSERT INTO homework VALUES(null,'"
										+ c.getString("Homework") + "','"
										+ c.getString("Faculty") + "','"
										+ c.getString("Date") + "','"
										+ c.getString("HomeworkType") + "','"
										+ c.getString("Imagename") + "','"
										+ c.getString("Subject") + "') ");
							}

						} catch (JSONException e) {
							e.printStackTrace();
							Log.d("Homework json error", e.getMessage());
						}
					}
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			return null;
		}

		@SuppressLint("SimpleDateFormat")
		@Override
		protected void onPostExecute(String result) {
			// pDialog.dismiss();
			// pDialog.cancel();

			pgrShowHomework.setVisibility(View.GONE);
			lblShowTextSync.setVisibility(View.GONE);
			listview.setVisibility(View.VISIBLE);

			FillDataonListView();

		}

	}

	class DownloadFileFromURL extends AsyncTask<String, String, String> {

		private ProgressDialog pDialogkk;

		/**
		 * Before starting background thread Show Progress Bar Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			pDialogkk = new ProgressDialog(FragmentHomework.this);
			pDialogkk.setMessage("Downloading file. Please wait...");
			pDialogkk.setIndeterminate(false);
			pDialogkk.setMax(100);
			pDialogkk.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
			pDialogkk.setCancelable(true);
			pDialogkk.show();

		}

		/**
		 * Downloading file in background thread
		 * */

		@Override
		protected String doInBackground(String... f_url) {
			int count;
			try {
				URL url = new URL(f_url[0]);
				URLConnection conection = url.openConnection();
				conection.connect();
				// getting file length
				int lenghtOfFile = conection.getContentLength();

				// input stream to read file - with 8k buffer
				InputStream input = new BufferedInputStream(url.openStream(),
						8192);

				// Output stream to write file
				OutputStream output = new FileOutputStream(
						"/sdcard/homework_image.jpg");

				byte data[] = new byte[1024];

				long total = 0;

				while ((count = input.read(data)) != -1) {
					total += count;
					// publishing the progress....
					// After this onProgressUpdate will be called
					publishProgress("" + (int) ((total * 100) / lenghtOfFile));

					// writing data to file
					output.write(data, 0, count);
				}

				// flushing output
				output.flush();

				// closing streams
				output.close();
				input.close();

			} catch (Exception e) {
				Log.e("Error: ", e.getMessage());
			}

			return null;
		}

		/**
		 * Updating progress bar
		 * */
		protected void onProgressUpdate(String... progress) {
			// setting progress percentage
			pDialogkk.setProgress(Integer.parseInt(progress[0]));
		}

		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		@Override
		protected void onPostExecute(String file_url) {
			// dismiss the dialog after the file was downloaded
			pDialogkk.dismiss();
			Toast.makeText(FragmentHomework.this,
					"File Has been Downloaded,Check in your Internal Storage",
					Toast.LENGTH_LONG).show();
			// Displaying downloaded image into image view
			// Reading image path from sdcard
			// String imagePath = Environment.getExternalStorageDirectory()
			// .toString() + "/downloadedfile.jpg";
			// setting downloaded into image view
		}

	}

	// ***************************************

}
