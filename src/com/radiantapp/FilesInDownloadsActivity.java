package com.radiantapp;

import java.io.BufferedInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.radiantapp.adapter.GalleryAlbumAdapter;
import com.radiantapp.adapter.GridViewImageAdapter;

public class FilesInDownloadsActivity extends Activity {
	private static final String TAG = FilesInDownloadsActivity.class
			.getSimpleName();
	private GridView mGridView;
	private ProgressBar mProgressBar;
	private GridViewImageAdapter mGridAdapter;
	private GalleryAlbumAdapter adapter;
	private ArrayList<ImageItemAlbum> mGridData;
	private String FEED_URL = "";
	String title;

	ImageItemAlbum tum;
	private String doclink, link, filename;
	private dbhandler db;
	private SQLiteDatabase sd;
	private String url;
	private JSONArray jarraygallery;
	ArrayList gallerylist;
	TextView back;
	private Dialog dialog;
	Button btnView, btnDownload;
	private String bigImageLink;
	ArrayList<HashMap<String, String>> doclist = new ArrayList<HashMap<String, String>>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_files_in_downloads);
		// getActionBar().hide();
		Intent i = getIntent();
		title = i.getStringExtra("ID");
		mGridView = (GridView) findViewById(R.id.gridViewDocuments);
		mProgressBar = (ProgressBar) findViewById(R.id.progressBarDocuments);

		gallerylist = new ArrayList<HashMap<String, String>>();
		db = new dbhandler(getApplicationContext());
		sd = db.getReadableDatabase();
		sd = db.getWritableDatabase();

		back = (TextView) findViewById(R.id.goback);

		new GetGalleryFromServer().execute();

		back.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(FilesInDownloadsActivity.this,
						DownloadsActivity.class);
				startActivity(intent);
				finish();
			}
		});

		mGridView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View v,
					int position, long arg3) {

				try {

					showDialogBox();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		});

	}

	private void showDialogBox() {
		try {

			// ***************************

			final Dialog dialog = new Dialog(FilesInDownloadsActivity.this);

			// setting custom layout to dialog
			dialog.setContentView(R.layout.prompt_dialog);
			dialog.setTitle("Select Option");

			// adding text dynamically
			btnView = (Button) dialog.findViewById(R.id.btnViewOnly);
			btnDownload = (Button) dialog.findViewById(R.id.btnDownloadOnly);

			// ***************************
			// dialog.setCancelable(false);
			dialog.show();

			btnView.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View view) {

					db = new dbhandler(FilesInDownloadsActivity.this);
					sd = db.getReadableDatabase();

					String qry = "select * from downloads where catid=" + title;
					Cursor c = sd.rawQuery(
							"select * from downloads where catid=" + title,
							null);

					while (c.moveToNext()) {

						doclink = c.getString(3);
					}

					try {

						if (doclink.toLowerCase().contains(".png")
								|| doclink.toLowerCase().contains(".jpg")) {

							Intent ik = new Intent(
									FilesInDownloadsActivity.this,
									DetailsOfPhoto.class);
							ik.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
							ik.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
							ik.putExtra("IMAGELINK", doclink);
							startActivity(ik);

						} else if (doclink.toLowerCase().contains(".pdf")
								|| doclink.toLowerCase().contains(".docx")) {

							doclink = doclink.substring(7);

							Intent iq = new Intent(
									FilesInDownloadsActivity.this,
									DocDetailsActivity.class);
							iq.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
							iq.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
							iq.putExtra("FILE", doclink);

							startActivity(iq);
						}

					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}
			});

			btnDownload.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {

					try {
						dialog.dismiss();

						db = new dbhandler(FilesInDownloadsActivity.this);
						sd = db.getReadableDatabase();

						Cursor c = sd.rawQuery(
								"select * from downloads where catid=" + title,
								null);

						while (c.moveToNext()) {

							link = c.getString(3);
							filename = c.getString(4);
						}

						new DownloadFileFromURL().execute(link);

					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			});
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.files_in_downloads, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	// *****************************

	class GetGalleryFromServer extends AsyncTask<Void, Void, Void> {
		private String url1;

		private ProgressDialog pDialog1;

		@Override
		protected void onPreExecute() {
			pDialog1 = new ProgressDialog(FilesInDownloadsActivity.this);
			pDialog1.setMessage("Please wait...");
			pDialog1.show();
			pDialog1.setCancelable(false);
			// mProgressBar.setVisibility(View.VISIBLE);
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... urls) {

			JSONParser jParser = new JSONParser();

			// getting JSON string from URL=
			// url = AllKeys.WEBSITE +
			// "Json_data.aspx?type=upcomingtest&deptid="
			// + deptid;

			url = AllKeys.WEBSITE + "GetDownloadsFileData?type=downloads"
					+ "&clientid=" + AllKeys.CLIENT_ID + "&branch=0";
			ServiceHandler sh = new ServiceHandler();
			String str_stud_download = sh.makeServiceCall(url,
					ServiceHandler.GET);
			if (str_stud_download != null && !str_stud_download.equals("")) {

				str_stud_download = "{" + '"' + "Downloads" + '"' + ":"
						+ str_stud_download + "}";
				try {
					JSONObject json = new JSONObject(str_stud_download);
					if (json != null) {
						try {
							// Getting Array of Contacts
							jarraygallery = json.getJSONArray("Downloads");

							// looping through All Contacts
							sd.delete("downloads", null, null);
							for (int i = 0; i < jarraygallery.length(); i++) {
								JSONObject c = jarraygallery.getJSONObject(i);

								String docid = c.getString("ID");
								String title = c.getString("Title");
								String catId = c.getString("CatId");
								String link = c.getString("Link");
								String docfile = c.getString("FileName");

								try {
									sd.execSQL("INSERT INTO downloads VALUES("
											+ "'" + c.getString("ID") + "','"
											+ c.getString("Title") + "','"
											+ c.getString("CatId") + "','"
											+ c.getString("Link") + "','"
											+ c.getString("FileName") + "') ");
								} catch (SQLException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
								tum = new ImageItemAlbum();

								tum.setTitle(title);
								tum.setImage(docfile);
								// tum.setThumbUnderAlbum(albumphotothumb);
								gallerylist.add(tum);
							}

						} catch (JSONException e) {
							e.printStackTrace();
							Log.d("Albums json error", e.getMessage());
						}
					}
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			return null;
		}

		@SuppressLint("SimpleDateFormat")
		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			// backupDB();
			if (pDialog1.isShowing()) {

				pDialog1.dismiss();
				pDialog1.cancel();

			}
			adapter = new GalleryAlbumAdapter(getApplicationContext(),
					R.layout.grid_item_album_laoyut, gallerylist);
			mGridView.setAdapter(new GalleryAlbumAdapter(
					getApplicationContext(), R.layout.grid_item_album_laoyut,
					gallerylist));
		}
	}

	class DownloadFileFromURL extends AsyncTask<String, String, String> {

		private ProgressDialog pDialogkk;

		/**
		 * Before starting background thread Show Progress Bar Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			pDialogkk = new ProgressDialog(FilesInDownloadsActivity.this);
			pDialogkk.setMessage("Downloading file. Please wait...");
			pDialogkk.setIndeterminate(false);
			pDialogkk.setMax(100);
			pDialogkk.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
			pDialogkk.setCancelable(true);
			pDialogkk.show();

		}

		@Override
		protected String doInBackground(String... f_url) {
			int count;

			try {
				URL url = new URL(f_url[0]);
				if (url.equals("") || url.equals(null)) {
					Toast.makeText(getApplicationContext(), "Not Found",
							Toast.LENGTH_SHORT).show();
				} else {

					URLConnection conection = url.openConnection();
					conection.connect();
					// getting file length
					int lenghtOfFile = conection.getContentLength();

					// input stream to read file - with 8k buffer
					InputStream input = new BufferedInputStream(
							url.openStream(), 8192);

					// Output stream to write file
					OutputStream output = new FileOutputStream("/sdcard/"
							+ filename);

					byte data[] = new byte[1024];

					long total = 0;

					while ((count = input.read(data)) != -1) {
						total += count;
						// publishing the progress....
						// After this onProgressUpdate will be called
						publishProgress(""
								+ (int) ((total * 100) / lenghtOfFile));

						// writing data to file
						output.write(data, 0, count);
					}

					// flushing output
					output.flush();

					// closing streams
					output.close();
					input.close();
				}

			} catch (Exception e) {
				Log.e("Error: ", e.getMessage());
			}

			return null;
		}

		/**
		 * Updating progress bar
		 * */
		protected void onProgressUpdate(String... progress) {
			// setting progress percentage
			pDialogkk.setProgress(Integer.parseInt(progress[0]));
		}

		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		@Override
		protected void onPostExecute(String file_url) {
			// dismiss the dialog after the file was downloaded
			pDialogkk.dismiss();
			Toast.makeText(FilesInDownloadsActivity.this,
					"File Has been Downloaded,Check in your Internal Storage",
					Toast.LENGTH_LONG).show();
			// Displaying downloaded image into image view
			// Reading image path from sdcard
			// String imagePath = Environment.getExternalStorageDirectory()
			// .toString() + "/downloadedfile.jpg";
			// setting downloaded into image view
		}

	}
}
