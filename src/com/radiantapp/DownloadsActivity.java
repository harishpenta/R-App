package com.radiantapp;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.radiantapp.adapter.GalleryAlbumAdapter;

public class DownloadsActivity extends ActionBarActivity {

	ArrayList gallerylist;

	private String url;
	private JSONArray jarraygallery;
	private String catid, catname;
	private dbhandler db;
	private SQLiteDatabase sd;
	private GalleryAlbumAdapter adapter;
	private ListView listviewalbum;
	ImageItemAlbum tum;
	private Object albumId;
	ArrayList<HashMap<String, String>> catlist = new ArrayList<HashMap<String, String>>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		try {
			setContentView(R.layout.activity_downloads);
			getSupportActionBar().hide();

			listviewalbum = (ListView) findViewById(R.id.listviewDownloadsCategory);

			db = new dbhandler(getApplicationContext());
			sd = db.getReadableDatabase();
			sd = db.getWritableDatabase();
			if (NetConnectivity.isOnline(getApplicationContext())) {
				new GetGalleryFromServer().execute("");
			} else {
				FillDataonListView();
			}

			listviewalbum.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> arg0, View v, int arg2,
						long arg3) {

					String title = ((TextView) v.findViewById(R.id.textCatId))
							.getText().toString();

					Intent i = new Intent(DownloadsActivity.this,
							FilesInDownloadsActivity.class);
					i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					i.putExtra("ID", title);
					startActivity(i);

				}
			});

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.downloads, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	public void FillDataonListView() {

		try {
			Cursor c = sd.rawQuery("select * from categorydownload", null);

			// homeworklist.clear();
			int cnt = 0;
			while (c.moveToNext()) {

				catid = c.getString(0);
				catname = c.getString(1);

				HashMap<String, String> map = new HashMap<String, String>();

				map.put("CATID", catid);
				map.put("CATNAME", catname);

				catlist.add(map);
			}

			SimpleAdapter adapter = new SimpleAdapter(getApplicationContext(),
					catlist, R.layout.main_album_item, new String[] {
							"CATNAME", "CATID" }, new int[] { R.id.textCatName,
							R.id.textCatId });

			listviewalbum.setAdapter(adapter);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	class GetGalleryFromServer extends AsyncTask<String, Void, String> {
		private String url1;
		private ProgressDialog pDialog;

		@Override
		protected void onPreExecute() {
			Toast.makeText(getApplicationContext(), "Loading..",
					Toast.LENGTH_LONG).show();
			super.onPreExecute();
		}

		@Override
		protected String doInBackground(String... urls) {

			JSONParser jParser = new JSONParser();
			// getting JSON string from URL=
			// url = AllKeys.WEBSITE +
			// "Json_data.aspx?type=upcomingtest&deptid="
			// + deptid;

			// url = AllKeys.MAIN_WEB + "Json_data.aspx?type=filecat";
			url = AllKeys.WEBSITE + "GetFilesCategoryData?type=filecat"
					+ "&clientid=" + AllKeys.CLIENT_ID;
			ServiceHandler sh = new ServiceHandler();
			String str_stud_file = sh.makeServiceCall(url, ServiceHandler.GET);
			if (str_stud_file != null && !str_stud_file.equals("")) {

				str_stud_file = "{" + '"' + "category" + '"' + ":"
						+ str_stud_file + "}";
				// JSONObject json = jParser.getJSONFromUrl(url);
				try {
					JSONObject json = new JSONObject(str_stud_file);
					if (json != null) {
						try {
							// Getting Array of Contacts
							jarraygallery = json.getJSONArray("category");

							// looping through All Contacts
							sd.delete("categorydownload", null, null);
							for (int i = 0; i < jarraygallery.length(); i++) {
								JSONObject c = jarraygallery.getJSONObject(i);
								try {
									sd.execSQL("INSERT INTO categorydownload VALUES('"
											+ c.getString("ID")
											+ "','"
											+ c.getString("Category") + "') ");
								} catch (SQLException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}

							}

						} catch (JSONException e) {
							e.printStackTrace();
							Log.d("Albums json error", e.getMessage());
						}
					}
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			return null;
		}

		@SuppressLint("SimpleDateFormat")
		@Override
		protected void onPostExecute(String result) {

			FillDataonListView();
			// backupDB();

			// getAllData();

		}

	}
}
