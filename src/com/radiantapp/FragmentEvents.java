package com.radiantapp;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.TextView;

public class FragmentEvents extends Activity {

	private WebView wvsocial;
	private Context context = getApplicationContext();

	ProgressBar pgrShowAcademicSync;
	TextView lblShowAcademicText, backEvents;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.fragment_socialmedia);
		try {

			// Toast.makeText(getApplicationContext(), "Please wait.... ",
			// Toast.LENGTH_SHORT).show();
			wvsocial = (WebView) findViewById(R.id.wvsocial);
			pgrShowAcademicSync = (ProgressBar) findViewById(R.id.pgrShowAcademicSync);
			lblShowAcademicText = (TextView) findViewById(R.id.lblSyncTextAcademic);

			backEvents = (TextView) findViewById(R.id.backEvents);
			String url1 = "" + AllKeys.WEBSITE_EVENTS;
			pgrShowAcademicSync.setVisibility(View.VISIBLE);
			lblShowAcademicText.setVisibility(View.VISIBLE);
			wvsocial.setVisibility(View.GONE);
			startWebView(url1);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		backEvents.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent i = new Intent(FragmentEvents.this, MainActivity.class);
				i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(i);

			}
		});
	}

	private void startWebView(String url) {

		// Create new webview Client to show progress dialog
		// When opening a url or click on link

		wvsocial.setWebViewClient(new WebViewClient() {
			ProgressDialog progressDialog;

			// If you will not use this method url links are opeen in new brower
			// not in webview
			public boolean shouldOverrideUrlLoading(WebView view, String url) {
				view.loadUrl(url);
				return true;
			}

			// Show loader on url load
			public void onLoadResource(WebView view, String url) {
				// if (progressDialog == null) {
				// // in standard case YourActivity.this
				// progressDialog = new ProgressDialog(getApplicationContext());
				// progressDialog.setMessage("Loading...");
				// progressDialog.show();
				// }
			}

			public void onPageFinished(WebView view, String url) {
				try {
					pgrShowAcademicSync.setVisibility(View.GONE);
					lblShowAcademicText.setVisibility(View.GONE);
					wvsocial.setVisibility(View.VISIBLE);
				} catch (Exception exception) {
					exception.printStackTrace();
				}
			}
		});

		// Javascript inabled on webview
		wvsocial.getSettings().setJavaScriptEnabled(true);

		wvsocial.reload();
		wvsocial.loadUrl(url);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		return super.onOptionsItemSelected(item);
	}
}