package com.radiantapp;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

public class FragmentRemark extends Activity {

	ArrayList<HomeWork> homeworklista = new ArrayList<HomeWork>();
	ArrayList<HashMap<String, String>> homeworklist = new ArrayList<HashMap<String, String>>();

	Activity a;
	private String deptid;
	private SessionManager sessionmanager;
	private HashMap<String, String> userDetails;
	TextView lblBackRemarks;
	private ListView listview;
	Menu mymenu;
	private Cursor c;
	private String urls;
	private String url;
	private JSONArray jarrayremark;
	private String remark, facultyname, date;
	private dbhandler db;
	private SQLiteDatabase sd;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_remarks);
		try {
			sessionmanager = new SessionManager(getApplicationContext());
			userDetails = new HashMap<String, String>();

			userDetails = sessionmanager.getUserDetails();
			db = new dbhandler(getApplicationContext());
			sd = db.getReadableDatabase();
			sd = db.getWritableDatabase();

			deptid = userDetails.get(sessionmanager.KEY_DEPTID);
			lblBackRemarks = (TextView) findViewById(R.id.lblBackRemarks);
			listview = (ListView) findViewById(R.id.lstViewremark);

			if (NetConnectivity.isOnline(getApplicationContext())) {
				new GetRemarkDetailsFromServer().execute("");
			} else {
				FillDataonListView();
			}
			
			lblBackRemarks.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					Intent i = new Intent(FragmentRemark.this,
							MainActivity.class);
					i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					startActivity(i);

				}
			});
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void FillDataonListView() {

		dbhandler db = new dbhandler(getApplicationContext());
		SQLiteDatabase sd = db.getReadableDatabase();

		Cursor c = sd.rawQuery("select * from remarkmst", null);

		homeworklist.clear();
		while (c.moveToNext()) {

			remark = c.getString(3);
			facultyname = c.getString(2);
			date = c.getString(1);

			HashMap<String, String> map = new HashMap<String, String>();

			map.put("REMARK", remark);
			map.put("FACULTY", "Teacher : " + facultyname);
			map.put("DATE", date);

			homeworklist.add(map);
		}

		SimpleAdapter adapter = new SimpleAdapter(getApplicationContext(),
				homeworklist, R.layout.lst_row_single_remark, new String[] {
						"REMARK", "FACULTY", "DATE" }, new int[] {
						R.id.lblRemark, R.id.lblFacultyRemark,
						R.id.lblDateRemark });

		listview.setAdapter(adapter);

	}

	// ***************************************

	class GetRemarkDetailsFromServer extends AsyncTask<String, Void, String> {

		private String url1;
		private ProgressDialog pDialog;

		@Override
		protected void onPreExecute() {
			pDialog = new ProgressDialog(FragmentRemark.this);

			pDialog.setMessage("Please wait...");
			pDialog.show();
			pDialog.setCancelable(false);
			super.onPreExecute();
		}

		@Override
		protected String doInBackground(String... urls) {

			url = AllKeys.WEBSITE
					+ "GetStudRemarkDetailByStudData?type=studremark&studid="
					+ userDetails.get(SessionManager.KEY_EMPID) + "&clientid="
					+ AllKeys.CLIENT_ID + "&branch="
					+ userDetails.get(sessionmanager.KEY_BRANCHID);
			Log.d("remark", url);
			ServiceHandler sh = new ServiceHandler();
			String str_stud_remark = sh
					.makeServiceCall(url, ServiceHandler.GET);

			ContentValues cv = new ContentValues();
			if (str_stud_remark != null && !str_stud_remark.equals("")) {

				str_stud_remark = "{" + '"' + "StudRemarks" + '"' + ":"
						+ str_stud_remark + "}";

				try {
					JSONObject json = new JSONObject(str_stud_remark);

					if (json != null) {
						try {
							Log.d("Remark Json data", json.toString());
							// Getting Array of Contacts
							jarrayremark = json.getJSONArray("StudRemarks");

							// looping through All Contacts
							sd.delete("remarkmst", null, null);
							for (int i = 0; i < jarrayremark.length(); i++) {
								JSONObject c = jarrayremark.getJSONObject(i);

								sd.execSQL("INSERT INTO remarkmst VALUES(null,'"
										+ c.getString("Date")
										+ "','"
										+ c.getString("FacultyID")
										+ "','"
										+ c.getString("Remark") + "') ");
							}

						} catch (JSONException e) {
							e.printStackTrace();
							Log.d("Remark json error", e.getMessage());
						}
					}
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}

			return null;
		}

		@SuppressLint("SimpleDateFormat")
		@Override
		protected void onPostExecute(String result) {
			pDialog.dismiss();
			pDialog.cancel();

			FillDataonListView();

		}

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		return super.onOptionsItemSelected(item);
	}
	// ***************************************
}
