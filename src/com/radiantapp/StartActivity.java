package com.radiantapp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.Html;
import android.util.Log;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ExpandableListView.OnGroupClickListener;
import android.widget.Toast;
import android.os.Build;

public class StartActivity extends ActionBarActivity {

	private ExpandableListView mexpandablistview;
	ArrayList<Parent> arrayParents = new ArrayList<Parent>();
	ArrayList<String> arrayChildren = new ArrayList<String>();
	private Context context = this;
	private JSONParser jParser;
	private String url;
	private JSONObject json2;
	private JSONArray jarrayholiday;

	double latitude = 21.202560;
	double longitude = 72.789358;

	private GoogleMap googleMap;
	private MapFragment mMapFragment;

	double loc_latitude = 21.1645104;
	double loc_longitude = 72.7713561;

	private SessionManager sessionManager;

	private HashMap<String, String> userDetails;

	private Timer timer;
	private TimerTask timerTask;
	private Handler handler = new Handler();
	private int interval = 3000;
	private int BannerCounter = 0;

	private ViewPager viewPager;
	private TextView txtdns;

	SQLiteDatabase sd;
	private WebView wv;
	private Button btnlogin;
	private TextView txtnet;
	public String urlkk;

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_start);

		String title = "Radiant";
		try {
			// new DownloadWebPageTask().execute();
			sessionManager = new SessionManager(context);
			userDetails = new HashMap<String, String>();
			userDetails = sessionManager.getUserDetails();
			latitude = Double.parseDouble(userDetails
					.get(SessionManager.KEY_LATTITUDE));
			longitude = Double.parseDouble(userDetails
					.get(SessionManager.KEY_LONGTITUDE));
		} catch (NumberFormatException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		try {
			if (android.os.Build.VERSION.SDK_INT > 11) {
				// getSupportActionBar().setTitle("IMS");
				setTitle(Html.fromHtml("<font color='#FFFFFF'>" + title
						+ "</font>"));

				// getSupportActionBar().setDisplayHomeAsUpEnabled(true);
				// getSupportActionBar().setHomeButtonEnabled(true);
				getSupportActionBar().hide();
				getSupportActionBar()
						.setBackgroundDrawable(
								new ColorDrawable(Color
										.parseColor(AllKeys.THEME_COLOR)));

			} else {
				// getActionBar().setTitle("IMS");
				setTitle(Html.fromHtml("<font color='#FFFFFF'>" + title
						+ "</font>"));
				getActionBar()
						.setBackgroundDrawable(
								new ColorDrawable(Color
										.parseColor(AllKeys.THEME_COLOR)));
				// getActionBar().setDisplayHomeAsUpEnabled(true);
				// getActionBar().setHomeButtonEnabled(true);
				getActionBar().hide();

			}
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		try {

			if (NetConnectivity.isOnline(context)) {
				// Loading map
				// initilizeMap();
			} else {

				Toast.makeText(getApplicationContext(),
						"Please Check Network Connection", Toast.LENGTH_LONG)
						.show();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		// getSupportActionBar().setBackgroundDrawable(new
		// ColorDrawable(Color.parseColor("#44223a")));
		// getSupportActionBar().hide();
		txtdns = (TextView) findViewById(R.id.txtdns);
		Typeface font = Typeface.createFromAsset(getAssets(),
				"OpenSans-Light.ttf");// RobotoCondensed-Light.ttf
		txtdns.setTypeface(font);
		// initViewPager();
		dbhandler db = new dbhandler(context);
		sd = db.getReadableDatabase();
		jParser = new JSONParser();
		btnlogin = (Button) findViewById(R.id.btnlogin);
		txtnet = (TextView) findViewById(R.id.txtnet);
		wv = (WebView) findViewById(R.id.webView1);
		wv.getSettings().setJavaScriptEnabled(true);

		if (NetConnectivity.isOnline(context)) {
			txtnet.setVisibility(View.GONE);
			Log.d("URL ContactUs : ", "http://" + AllKeys.MAIN_WEB
					+ "/mobcontent.aspx?type=menu");
			wv.loadUrl("http://" + AllKeys.MAIN_WEB
					+ "/mobcontent.aspx?type=menu");
		} else {

		}

		btnlogin.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent ii = new Intent(context, LoginActivity.class);
				startActivity(ii);
				finish();
			}
		});

		try {
			// new DownloadWebPageTask().execute();

		} catch (Exception e) {
			System.out.print("Errror :" + e.getMessage());

		}

		/*
		 * // getting JSON string from URL
		 * url="http://www.modulus.ac.in/json_data.aspx?type=Subject"; json2 =
		 * jParser.getJSONFromUrl(url);
		 * 
		 * try { // Getting Array of Contacts jarrayholiday =
		 * json2.getJSONArray("SubjectMst");
		 * 
		 * // looping through All Contacts sd.delete("submaster", null, null);
		 * for(int i = 0; i < jarrayholiday.length(); i++) { JSONObject c =
		 * jarrayholiday.getJSONObject(i);
		 * sd.execSQL("INSERT INTO submaster VALUES('"
		 * +c.getString("code")+"','"+c.getString("name")+"') "); }
		 * 
		 * 
		 * } catch (JSONException e) { e.printStackTrace(); }
		 */
		mexpandablistview = (ExpandableListView) findViewById(R.id.expandableListView1);
		/*
		 * Cursor cs=sd.rawQuery("select * from MobileTitle", null);
		 * startManagingCursor(cs); if(cs.getCount()>0) { populatelists(); }
		 * else {
		 * 
		 * populatelist(); }
		 */
		mexpandablistview.setAdapter(new com.radiantapp.ExpandableListAdapter(
				context, arrayParents));
		mexpandablistview.setOnGroupClickListener(new OnGroupClickListener() {

			@Override
			public boolean onGroupClick(ExpandableListView parent, View v,
					int groupPosition, long id) {
				// TODO Auto-generated method stub

				Parent p = arrayParents.get(groupPosition);
				if (p.getTitle().contains("Login")) {
					// dbhandler db=new dbhandler(context);
					// SQLiteDatabase sd=db.getReadableDatabase();

					// Cursor chkdata=sd.rawQuery("select * from studinfo",
					// null);
					// startManagingCursor(chkdata);

					/*
					 * if(chkdata.getCount()>0) {
					 * 
					 * Intent i=new Intent(context,MainActivity.class);
					 * 
					 * startActivity(i); } else {
					 */
					Intent i = new Intent(context, LoginActivity.class);
					startActivity(i);
					// finish();
					// }
				}
				if (p.getTitle().contains("Video")) {
					Intent i = new Intent(context, VideoActivity.class);
					startActivity(i);
				}
				return false;
			}
		});

	}

	public void FillDataonExpandalbeListview() {
		Cursor cs = sd.rawQuery("select * from MobileTitle", null);
		startManagingCursor(cs);
		if (cs.getCount() > 0) {
			populatelists();
		} else {

			populatelist();
		}

	}

	public void populatelist() {
		arrayParents.clear();
		Parent parent = new Parent();
		dbhandler db = new dbhandler(context);
		SQLiteDatabase sd = db.getReadableDatabase();

		// Cursor cdept=sd.rawQuery("select * from studinfo", null);
		// startManagingCursor(cdept);

		for (int i = 0; i < 5; i++) {
			parent = new Parent();

			// parent.setTitle("      Heading"+i);

			// parent.setArrayChildren(arrayChildren);

			// arrayChildren = new ArrayList<String>();
			// arrayChildren.clear();
			// Cursor
			// c=sd.rawQuery("select * from time_tb_mst where day='"+days[i]+"' and deptid="+deptid,
			// null);
			// startManagingCursor(c);
			// while(c.moveToNext())

			{
				arrayChildren.add(" Title " + i);
			}
			parent.setArrayChildren(arrayChildren);

			// in this array we add the Parent object. We will use the
			// arrayParents at the setAdapter
			arrayParents.add(parent);
		}

		parent = new Parent();
		parent.setTitle("      Video");
		arrayChildren = new ArrayList<String>();
		parent.setArrayChildren(arrayChildren);
		arrayParents.add(parent);
		parent = new Parent();
		parent.setTitle("      Login");
		arrayChildren = new ArrayList<String>();
		parent.setArrayChildren(arrayChildren);
		arrayParents.add(parent);

	}

	public void populatelists() {
		arrayParents.clear();
		Parent parent = new Parent();
		dbhandler db = new dbhandler(context);
		SQLiteDatabase sd = db.getReadableDatabase();

		Cursor cdept = sd.rawQuery("select * from MobileTitle", null);
		startManagingCursor(cdept);

		while (cdept.moveToNext()) {
			parent = new Parent();

			parent.setTitle("      " + cdept.getString(1));

			parent.setArrayChildren(arrayChildren);

			arrayChildren = new ArrayList<String>();
			arrayChildren.clear();
			// Cursor
			// c=sd.rawQuery("select * from time_tb_mst where day='"+days[i]+"' and deptid="+deptid,
			// null);
			// startManagingCursor(c);
			// while(c.moveToNext())

			{
				arrayChildren.add("  " + cdept.getString(2));
			}
			parent.setArrayChildren(arrayChildren);

			// in this array we add the Parent object. We will use the
			// arrayParents at the setAdapter
			arrayParents.add(parent);
		}

		/*
		 * parent=new Parent(); parent.setTitle("      Video");
		 * arrayChildren=new ArrayList<String>();
		 * parent.setArrayChildren(arrayChildren); arrayParents.add(parent);
		 */
		parent = new Parent();
		parent.setTitle("      Login");
		arrayChildren = new ArrayList<String>();
		parent.setArrayChildren(arrayChildren);
		arrayParents.add(parent);
		mexpandablistview.setAdapter(new com.radiantapp.ExpandableListAdapter(
				context, arrayParents));

	}

	private void initViewPager() {
		// viewPager = (ViewPager)findViewById(R.id.view_pager);
		viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

			@Override
			public void onPageSelected(int pos) {
				viewPager.setCurrentItem(pos);
			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {

			}

			@Override
			public void onPageScrollStateChanged(int arg0) {

			}
		});
		ImagePagerAdapter adapter = new ImagePagerAdapter();
		viewPager.setAdapter(adapter);
		viewPager.setCurrentItem(BannerCounter);

	}

	// //For Loading the mobile site pages /////

	private class DownloadWebPageTask extends AsyncTask<String, Void, String> {

		private String url1;

		private String url;
		private JSONArray jarrayholiday;
		private JSONObject json2;
		private JSONObject json3;
		private JSONObject json4;
		private JSONObject json5;
		private JSONObject json6;

		private PendingIntent pendingIntent;

		private int m;

		private int min;

		ProgressDialog moProgress = new ProgressDialog(StartActivity.this);

		private JSONObject jsonnotes;

		private boolean flag;

		private String urlcontact;

		@Override
		protected void onPreExecute() {
			// moProgress.dismiss();
			if (!moProgress.isShowing()) {
				moProgress.setMessage("Please Wait Verifying ");
				moProgress.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
				moProgress.setIndeterminate(true);
				moProgress.show();
				flag = true;
			}
		}

		@Override
		protected String doInBackground(String... urls) {
			String response = "";

			String qry = "Select * from DataUrl";
			dbhandler db = new dbhandler(context);
			SQLiteDatabase sd = db.getReadableDatabase();
			Cursor cdata = sd.rawQuery(qry, null);
			startManagingCursor(cdata);
			while (cdata.moveToNext()) {
				// url1=cdata.getString(0);
			}

			JSONParser jParser = new JSONParser();

			// getting JSON string from URL
			// url1="http://ravisirbalajiclasses.com/";
			urlkk = AllKeys.WEBSITE
					+ "serviceP.asmx/GetJSONForContactUsData?type=contact&clientid="
					+ AllKeys.CLIENT_ID;
			ServiceHandler shcon = new ServiceHandler();
			String str_stud_contact = shcon.makeServiceCall(urlkk,
					ServiceHandler.GET);

			if (str_stud_contact != null && !str_stud_contact.equals("")) {

				str_stud_contact = "{" + '"' + "Contact" + '"' + ":"
						+ str_stud_contact + "}";
				// urlkk = AllKeys.WEBSITE
				// + "json_data.aspx?type=contact";
				try {
					JSONObject jsoncontact = new JSONObject(str_stud_contact);
					// json2 =
					// jParser.getJSONFromUrl(urlkk);

					if (jsoncontact != null) {
						try {

							// Getting Array of
							// Contacts
							jarrayholiday = jsoncontact.getJSONArray("Contact");

							// looping through All
							// Contacts
							// sd.delete("submaster",
							// null,
							// null);
							for (int i = 0; i < jarrayholiday.length(); i++) {
								JSONObject c = jarrayholiday.getJSONObject(i);

								String lat = c.getString("Latitude");
								String lon = c.getString("Longitude");
								sessionManager.createContactUsDetails(
										"" + c.getString("Latitude"),
										"" + c.getString("Longitude"),
										"" + c.getString("Address")
												+ ",,Mobile No : "
												+ c.getString("MobileNo")
												+ ",,Phone No : "
												+ c.getString("PhoneNo")
												+ ",,Email : "
												+ c.getString("Email"));

								// sd.execSQL("INSERT INTO submaster VALUES('"+c.getString("code")+"','"+c.getString("name")+"') ");
							}

						} catch (JSONException e) {
							e.printStackTrace();
							Log.d("contact json error", e.getMessage());
						}
					}
				} catch (Exception e) {
					// TODO Auto-generated catch
					// block
					e.printStackTrace();
				}
			}
			return response;

		}

		@Override
		protected void onPostExecute(String result) {

			try {
				FillDataonExpandalbeListview();

			} catch (Exception e) {
				System.out.print("" + e.getMessage());
			}
			moProgress.dismiss();

		}
	}

	// //Complete loading pages from over the website////

	@Override
	protected void onResume() {
		super.onResume();
		stopTimer();
		startTimer();
	}

	@Override
	protected void onStop() {
		stopTimer();
		super.onStop();
	}

	private void startTimer() {
		if (timer == null) {

			timer = new Timer();
			timerTask = new MyTimerTask();
			timer.schedule(timerTask, 0, interval);
		}
	}

	private void stopTimer() {
		if (timer != null) {
			timer.cancel();
			timer = null;
		}
	}

	public class MyTimerTask extends TimerTask {

		private Runnable runnable = new Runnable() {
			public void run() {
				try {

					if (BannerCounter == 5) {
						BannerCounter = 0;
					} else {
						BannerCounter++;
					}
					viewPager.setCurrentItem(BannerCounter);

				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		};

		public void run() {
			handler.post(runnable);
		}
	}

	private class ImagePagerAdapter extends PagerAdapter {
		// private ImageLoader imageLoader;

		public ImagePagerAdapter() {

		}

		@Override
		public int getCount() {
			return 5;
		}

		@Override
		public Object instantiateItem(ViewGroup container, int position) {

			ImageView imageView = new ImageView(context);

			return imageView;
		}

		@Override
		public void destroyItem(ViewGroup container, int position, Object object) {
			((ViewPager) container).removeView((ImageView) object);
		}

		@Override
		public boolean isViewFromObject(View arg0, Object arg1) {
			// TODO Auto-generated method stub
			return arg0 == ((ImageView) arg1);

		}
	}

	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			// do something on back.
			StartActivity.this.finish();// try activityname.finish instead of
										// this
			Intent intent = new Intent(Intent.ACTION_MAIN);
			intent.addCategory(Intent.CATEGORY_HOME);
			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

			startActivity(intent);

			return true;
		}

		return super.onKeyDown(keyCode, event);
	}

}
