package com.radiantapp;

import java.util.HashMap;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

public class TimeTableActivity extends Activity {
	private String empid, deptid;
	private WebView webView;
	SessionManager sessionmanager;
	HashMap<String, String> userDetails;
	ImageView imgErrorTime;
	ProgressBar pgr;
	TextView lblBackTimetable;

	protected void onCreate(Bundle savedInstanceState) {
		//
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_time_table);
		// getActionBar().hide();
		try {

			webView = (WebView) findViewById(R.id.webTimetable);
			// imgErrorTime = (ImageView) v.findViewById(R.id.imgErrorTime);
			sessionmanager = new SessionManager(getApplicationContext());
			userDetails = new HashMap<String, String>();
			pgr = (ProgressBar) findViewById(R.id.pgr);
			lblBackTimetable = (TextView) findViewById(R.id.lblBackTimetable);
			userDetails = sessionmanager.getUserDetails();
			empid = userDetails.get(SessionManager.KEY_EMPID);
			deptid = userDetails.get(SessionManager.KEY_DEPTID);
			ConnectionDetector cd = new ConnectionDetector(
					getApplicationContext());

			lblBackTimetable.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					Intent i = new Intent(TimeTableActivity.this,
							MainActivity.class);
					i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					startActivity(i);

				}
			});
			if (cd.isConnectingToInternet() == true) {
				// new DownloadWebPageTask().execute("");
				// getalldata();
				startWebView("http://radiant.dnsitexperts.com/TimeTable.aspx?deptid="
						+ deptid);

			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// listview=(ListView)v.findViewById(R.id.listView1);
		// mExpandableList = (ExpandableListView) v
		// .findViewById(R.id.expandableListView1);
		// //populatelist();
		//
		// mExpandableList.setAdapter(new ExpandableListAdapter(getActivity(),
		// arrayParents));

	}

	// ***********************************************************************

	private void startWebView(String url) {

		// Create new webview Client to show progress dialog
		// When opening a url or click on link

		webView.setWebViewClient(new WebViewClient() {

			// If you will not use this method url links are opeen in new brower
			// not in webview
			public boolean shouldOverrideUrlLoading(WebView view, String url) {
				view.loadUrl(url);
				return true;
			}

			// Show loader on url load
			public void onLoadResource(WebView view, String url) {
				try {
					pgr.setVisibility(View.VISIBLE);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			public void onPageFinished(WebView view, String url) {
				try {
					pgr.setVisibility(View.GONE);
				} catch (Exception exception) {
					exception.printStackTrace();
				}
			}
		});

		// Javascript inabled on webview
		try {
			webView.getSettings().setJavaScriptEnabled(true);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// Other webview options
		/*
		 * webView.getSettings().setLoadWithOverviewMode(true);
		 * webView.getSettings().setUseWideViewPort(true);
		 * webView.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
		 * webView.setScrollbarFadingEnabled(false);
		 * webView.getSettings().setBuiltInZoomControls(true);
		 */
		/*
		 * String summary =
		 * "<html><body>You scored <b>192</b> points.</body></html>";
		 * webview.loadData(summary, "text/html", null);
		 */

		// Load url in webview
		webView.loadUrl(url);

	}

}
