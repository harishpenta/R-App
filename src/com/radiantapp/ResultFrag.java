package com.radiantapp;

import java.util.HashMap;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

public class ResultFrag extends Activity {

	private String empid, deptid;
	private WebView webView;

	SessionManager sessionManager;
	HashMap<String, String> userDetails;
	dbhandler dh;
	SQLiteDatabase sd;
	ConnectionDetector cd;
	ProgressBar pgrShoeResult;
	TextView lblShowResultText, backResult;
	ImageView imgErrorResult;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.resultxml);

		try {
			webView = (WebView) findViewById(R.id.webResults);
			backResult = (TextView) findViewById(R.id.backResult);
			pgrShoeResult = (ProgressBar) findViewById(R.id.pgrShowResultSync);
			lblShowResultText = (TextView) findViewById(R.id.lblSyncResultAcademic);
			// imgErrorResult=(ImageView)v.findViewById(R.id.imgErrorResult);
			sessionManager = new SessionManager(getApplicationContext());
			userDetails = new HashMap<String, String>();
			userDetails = sessionManager.getUserDetails();

			empid = userDetails.get(SessionManager.KEY_EMPID);
			deptid = userDetails.get(SessionManager.KEY_DEPTID);
			cd = new ConnectionDetector(getApplicationContext());
			if (cd.isConnectingToInternet() == true) {

				pgrShoeResult.setVisibility(View.VISIBLE);
				lblShowResultText.setVisibility(View.VISIBLE);
				webView.setVisibility(View.GONE);
				startWebView("http://radiant.dnsitexperts.com/result.aspx?deptid="
						+ deptid + "&empid=" + empid);
			}

			backResult.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					Intent i = new Intent(ResultFrag.this, MainActivity.class);
					i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					startActivity(i);

				}
			});
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void startWebView(String url) {

		// Create new webview Client to show progress dialog
		// When opening a url or click on link

		webView.setWebViewClient(new WebViewClient() {
			ProgressDialog progressDialog;

			// If you will not use this method url links are opeen in new brower
			// not in webview
			public boolean shouldOverrideUrlLoading(WebView view, String url) {
				view.loadUrl(url);
				return true;
			}

			// Show loader on url load
			public void onLoadResource(WebView view, String url) {
				// if (progressDialog == null) {
				// // in standard case YourActivity.this
				// progressDialog = new ProgressDialog(getActivity());
				// progressDialog.setMessage("Loading...");
				// progressDialog.show();
				// }

			}

			public void onPageFinished(WebView view, String url) {
				try {
					pgrShoeResult.setVisibility(View.GONE);
					lblShowResultText.setVisibility(View.GONE);
					webView.setVisibility(View.VISIBLE);
				} catch (Exception exception) {
					exception.printStackTrace();
				}
			}

		});

		// Javascript inabled on webview
		webView.getSettings().setJavaScriptEnabled(true);

		// Other webview options
		/*
		 * webView.getSettings().setLoadWithOverviewMode(true);
		 * webView.getSettings().setUseWideViewPort(true);
		 * webView.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
		 * webView.setScrollbarFadingEnabled(false);
		 * webView.getSettings().setBuiltInZoomControls(true);
		 */

		/*
		 * String summary =
		 * "<html><body>You scored <b>192</b> points.</body></html>";
		 * webview.loadData(summary, "text/html", null);
		 */

		// Load url in webview
		webView.loadUrl(url);
		{

		}

	}

}
