package com.radiantapp;

import java.util.ArrayList;

import com.radiantapp.adapter.GridViewImageAdapter;
import com.radiantapp.helper.AppConstant;
import com.radiantapp.helper.Utils;

import android.R.mipmap;
import android.annotation.TargetApi;
import android.app.ActionBar.LayoutParams;
import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.database.Cursor;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Gallery;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Toast;

public class GallaryFrag extends Fragment {

	Activity a;
	ImageView selectedImage;
	private Integer[] mImageIds = { R.drawable.ic_action_delete,
			R.drawable.ic_action_edit, R.drawable.ic_drawer,
			R.drawable.ic_launcher, R.drawable.notes, R.drawable.saveicon,
			R.drawable.sactivity, R.drawable.newcontent, R.drawable.results,
			R.drawable.notes, R.drawable.newcontent, R.drawable.saveicon, };

	LinearLayout imageView;

	private Utils utils;
	private ArrayList<String> imagePaths = new ArrayList<String>();
	private GridViewImageAdapter adapter;
	private GridView gridView;
	private int columnWidth;

	ArrayList<HolidayItem> holidaylist = new ArrayList<HolidayItem>();
	private GridView gv;

	public static GallaryFrag create() {
		GallaryFrag f = new GallaryFrag();
		return f;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.gallaryxml, container, false);

		gridView = (GridView) v.findViewById(R.id.gridView1);

		utils = new Utils(getActivity());

		// Initilizing Grid View
		InitilizeGridLayout();

		// loading all image paths from SD card
		imagePaths = utils.getFilePaths();

		// Gridview adapter
		adapter = new GridViewImageAdapter(getActivity(), imagePaths,
				columnWidth);

		// setting grid view adapter
		gridView.setAdapter(adapter);
		return v;
	}

	private void InitilizeGridLayout() {
		Resources r = getResources();
		float padding = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
				AppConstant.GRID_PADDING, r.getDisplayMetrics());

		columnWidth = (int) ((utils.getScreenWidth() - ((AppConstant.NUM_OF_COLUMNS + 1) * padding)) / AppConstant.NUM_OF_COLUMNS);

		gridView.setNumColumns(AppConstant.NUM_OF_COLUMNS);
		gridView.setColumnWidth(columnWidth);
		gridView.setStretchMode(GridView.NO_STRETCH);
		gridView.setPadding((int) padding, (int) padding, (int) padding,
				(int) padding);
		gridView.setHorizontalSpacing((int) padding);
		gridView.setVerticalSpacing((int) padding);
	}

}
