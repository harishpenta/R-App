package com.radiantapp;


public class AllKeys {

	public static final String WEBSITE = "http://panel.dnsitexperts.com/serviceP.asmx/";// http://ravisirbalajiclasses.com/

	public static final String WEBSITE2 = "http://panel.dnsitexperts.com/";//
	public static final String CLIENT_ID = "36";//
	public static final String MAIN_WEB = "radiant.dnsitexperts.com";//
	public static final String WEBSITE_SOCIALLINK = "http://radiant.dnsitexperts.com/sociallinks.aspx";

	public static final String WEBSITE_EVENTS = "http://radiant.dnsitexperts.com/events.aspx";

	public static final String TAG_MOBILEVIDEOSARRAY = "Video";
	public static final String TAG_VIDEONAME = "VideoName";
	public static final String TAG_VIDEOURL = "VideoUrl";
	public static final String TAG_THUMBNAIL = "ThumbCode";
	public static final String TAG_VIDEOID = "VideoId";

	public static final String THEME_COLOR = "#223581";

	public static final String INTERNET_ERROR_MSG = "No able to connect to appname,Please check your network connection and try again";

	// All Questions Related Keys
	public static final String ARRAY_ALLQUESTION = "StudentQuestion";
	public static final String TAG_STUDENTID = "StudID";
	public static final String TAG_FACULTYID = "FacultyID";
	public static final String TAG_FACULTYNAME = "FacultyName";
	public static final String TAG_QUESTION = "Question";
	public static final String TAG_DATETIME = "DateTime";
	public static final String TAG_ANSWER_STATUS = "AnsStatus";
	public static final String TAG_QUESTION_ID = "QuesID";

}
