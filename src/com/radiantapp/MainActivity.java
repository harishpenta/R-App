package com.radiantapp;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.ActionBar.LayoutParams;
import android.app.AlarmManager;
import android.app.Dialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.text.Html;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends ActionBarActivity {

	AsyncTask<Void, Void, Void> mRegisterTask;
	boolean doubleBackToExitPressedOnce = false;
	private String[] mMenuTitles;
	private ListView mDrawerList;
	private DrawerLayout mDrawerLayout;
	private ActionBarDrawerToggle mDrawerToggle;
	private CharSequence mDrawerTitle;
	private CharSequence mTitle;
	private int pos = 100;
	private GridView gridview;
	String[] str = { "a", "d", "sad", "asd", "sda", "asdf", "asd", "asdfasd",
			"sadf", "", "asdf", "" };
	private Context context = this;
	ArrayList<Item> gridArray = new ArrayList<Item>();
	CustomGridViewAdaptor customGridAdapter;
	private GridView gridView;
	private String url;
	private String url1;
	private String deptid;
	private String empid;
	private JSONArray jarrayholiday;
	private String date;
	private MenuItem nametext;
	private String testid;
	private JSONObject jsonnotes;
	private SessionManager sessionmanager;
	private HashMap<String, String> userDetails;
	String time;
	ProgressBar prgShow;
	Menu mymenu;
	private ArrayList<String> lstMessage = new ArrayList<String>();
	String AppTitle = "Radiant";

	String ThemeColor = "";// #FF8C00
	private String CurrentDate = "";
	JSONParser json = new JSONParser();
	private dbhandler db;
	private SQLiteDatabase sd;
	TextView lblSyncText;
	String welcome_msg;

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_main);

		try {

			db = new dbhandler(context);
			sd = db.getReadableDatabase();
			sd = db.getWritableDatabase();

			// sd.delete("Notification_Mst", null, null);

			sessionmanager = new SessionManager(context);
			userDetails = new HashMap<String, String>();
			userDetails = sessionmanager.getUserDetails();
			prgShow = (ProgressBar) findViewById(R.id.pgrShow);
			lblSyncText = (TextView) findViewById(R.id.lblSyncText);

			if (userDetails.get(SessionManager.KEY_EMPID).equals("0")) {

				Intent intent = new Intent(getApplicationContext(),
						StartActivity.class);
				startActivity(intent);
			} else if (userDetails.get(SessionManager.KEY_VERSTATUS)
					.equals("0")) {

				Intent intent = new Intent(getApplicationContext(),
						VerificationActivity.class);
				startActivity(intent);
				finish();

			}

			ThemeColor = "#FF8C00";// #3F51B5
			try {
				getSupportActionBar().setDisplayHomeAsUpEnabled(true);
				getSupportActionBar().setHomeButtonEnabled(true);
				getSupportActionBar().show();
				getSupportActionBar()
						.setBackgroundDrawable(
								new ColorDrawable(Color
										.parseColor(AllKeys.THEME_COLOR)));
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			try {

				/*
				 * deptid = "" + ii.getStringExtra("DEPTID"); empid = "" +
				 * ii.getStringExtra("STUDENTID");
				 */

				deptid = userDetails.get(SessionManager.KEY_DEPTID);
				empid = userDetails.get(SessionManager.KEY_EMPID);

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		if (Integer.parseInt(userDetails.get(SessionManager.KEY_EMPID)) > 0
				&& !userDetails.get(SessionManager.KEY_EMPID).equals("null")) {

			try {

				time = getDateTime();

				if (time.contains(userDetails
						.get(SessionManager.KEY_CURRENT_DATE))
						&& !userDetails.get(SessionManager.KEY_CURRENT_DATE)
								.equals("")) {
					// Toast.makeText(context,
					// "Contain",Toast.LENGTH_SHORT).show();

				} else {
					if (NetConnectivity.isOnline(context)) {
						final ProgressDialog progress = ProgressDialog.show(
								MainActivity.this, "", "Please wait....", true,
								false);

						new Thread(new Runnable() {
							public void run() {
								// Write Statements Here

								CurrentDate = time.substring(0, 10);
								sessionmanager.CreateCurrentDate(""
										+ CurrentDate);

								time = time.replace(" ", "%20");
								time = time.replace("-", "%2D");
								time = time.replace(":", "%3A");

								String LoginCheckUrl = AllKeys.WEBSITE
										+ "json_data.aspx?type=tracklogin&empid="
										+ userDetails
												.get(SessionManager.KEY_EMPID)
										+ "&logintime=" + time + "";

								json.getJSONFromUrl(LoginCheckUrl);
								// System.out.print("Ans  :" + ans);
								progress.cancel();
							}
						}).start();

						// Toast.makeText(context,
						// "Not Contain"+time,Toast.LENGTH_SHORT).show();
					}
				}

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

		/*
		 * gridview=(GridView)findViewById(R.id.gridView1);
		 * 
		 * gridview.setAdapter(new
		 * ArrayAdapter<String>(context,android.R.layout.
		 * simple_list_item_1,str));
		 */

		// /////////Complete Checking GCM //////////
		try {
			// registerReceiver(mHandleMessageReceiver, new
			// IntentFilter(DISPLAY_MESSAGE_ACTION));
		} catch (Exception e) {
			System.out.print("Err : " + e.getMessage());
		}
		Bitmap attendece = BitmapFactory.decodeResource(this.getResources(),
				R.drawable.ic_attendance);
		Bitmap results = BitmapFactory.decodeResource(this.getResources(),
				R.drawable.ic_result);

		Paint circlePaint = new Paint();
		circlePaint.setAntiAlias(true);
		circlePaint.setColor(Color.RED);

		circlePaint.setStyle(Paint.Style.FILL);
		circlePaint.setAlpha(255);
		circlePaint.setStrokeWidth(10);

		Paint textPaint = new Paint();
		textPaint.setStyle(Style.FILL_AND_STROKE);
		textPaint.setColor(Color.WHITE);
		textPaint.setTextSize(12);
		textPaint.setFakeBoldText(true);

		Bitmap examsheet = BitmapFactory.decodeResource(this.getResources(),
				R.drawable.ic_testsolution);
		Bitmap holiday = BitmapFactory.decodeResource(this.getResources(),
				R.drawable.ic_event);
		Bitmap notification = BitmapFactory.decodeResource(this.getResources(),
				R.drawable.ic_notifications);
		Bitmap contactus = BitmapFactory.decodeResource(this.getResources(),
				R.drawable.ic_contactus);
		/*
		 * Bitmap onlinetest = BitmapFactory.decodeResource(this.getResources(),
		 * R.drawable.onlinetest);
		 */
		Bitmap timetable = BitmapFactory.decodeResource(this.getResources(),
				R.drawable.ic_timetable);
		/*
		 * Bitmap videos = BitmapFactory.decodeResource(this.getResources(),
		 * R.drawable.videos);
		 */
		Bitmap feedback = BitmapFactory.decodeResource(this.getResources(),
				R.drawable.ic_feedback);

		Bitmap profilepic = BitmapFactory.decodeResource(this.getResources(),
				R.drawable.ic_profile);

		Bitmap event = BitmapFactory.decodeResource(this.getResources(),
				R.drawable.ic_event);
		Bitmap noticeboard = BitmapFactory.decodeResource(this.getResources(),
				R.drawable.ic_noticeboard);
		Bitmap homework = BitmapFactory.decodeResource(this.getResources(),
				R.drawable.ic_homework);
		Bitmap socialmedia = BitmapFactory.decodeResource(this.getResources(),
				R.drawable.ic_socialmedia);
		// Bitmap rateapp = BitmapFactory.decodeResource(this.getResources(),
		// R.drawable.ic_rating);
		Bitmap achievements = BitmapFactory.decodeResource(this.getResources(),
				R.drawable.ic_achievements);
		Bitmap remarks = BitmapFactory.decodeResource(this.getResources(),
				R.drawable.ic_remark);
		Bitmap businfo = BitmapFactory.decodeResource(this.getResources(),
				R.drawable.ic_businfo);
		Bitmap parentlinks = BitmapFactory.decodeResource(this.getResources(),
				R.drawable.ic_links);
		Bitmap downloads = BitmapFactory.decodeResource(this.getResources(),
				R.drawable.ic_downloads);

		Bitmap leave = BitmapFactory.decodeResource(this.getResources(),
				R.drawable.ic_leavedetails);
		// gridArray.add(new Item(leave, "Leave Details"));
		gridArray.clear();
		gridArray.add(new Item(profilepic, "Profile"));
		gridArray.add(new Item(attendece, "Attendance"));
		gridArray.add(new Item(results, "Result"));
		gridArray.add(new Item(timetable, "Time Table"));
		gridArray.add(new Item(homework, "Homework"));
		// gridArray.add(new Item(achievements, "Achievements"));
		gridArray.add(new Item(remarks, "Remarks"));
		gridArray.add(new Item(event, "Academic Calendar"));
		gridArray.add(new Item(noticeboard, "Notice Board"));
		gridArray.add(new Item(businfo, "Bus Info."));
		gridArray.add(new Item(notification, "Notification hub"));
		gridArray.add(new Item(feedback, "Feedback"));
		gridArray.add(new Item(contactus, "Contact Us"));
		gridArray.add(new Item(parentlinks, "Useful Links"));
		gridArray.add(new Item(downloads, "Downloads"));
		gridArray.add(new Item(leave, "Leave Details"));

		gridView = (GridView) findViewById(R.id.gridView1);
		customGridAdapter = new CustomGridViewAdaptor(context,
				R.layout.gridrow, gridArray);
		gridView.setAdapter(customGridAdapter);

		gridView.setOnItemClickListener(new OnItemClickListener() {

			private int HELLO_ID;

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				mDrawerToggle.setDrawerIndicatorEnabled(false);

				// Highlight the selected item, update the title, and close the
				// drawer
				mDrawerList.setItemChecked(position, true);
				// setTitle(mPlanetTitles[position]);
				mDrawerLayout.closeDrawer(mDrawerList);
				pos = position;
				if (position == 0) {
					// setTitle("Attendence");

					Intent i = new Intent(MainActivity.this,
							FragmentProfile.class);
					startActivity(i);

				}

				else if (position == 1) {

					Intent i = new Intent(MainActivity.this,
							AttendenceFrag.class);
					startActivity(i);

				} else if (position == 2) {
					// setTitle("Time Table");
					if (NetConnectivity.isOnline(context)) {
						Intent i = new Intent(MainActivity.this,
								ResultFrag.class);
						startActivity(i);

					} else {
						showDialog();
					}

				} else if (position == 3) {
					if (NetConnectivity.isOnline(context)) {
						Intent i = new Intent(MainActivity.this,
								TimeTableActivity.class);
						startActivity(i);

					} else {
						showDialog();
					}

				} else if (position == 4) {

					Intent i = new Intent(MainActivity.this,
							FragmentHomework.class);
					startActivity(i);
					// Toast.makeText(context, "Achievements",
					// Toast.LENGTH_SHORT).show();
				}

				else if (position == 5) {

					/*
					 * Toast.makeText(context, "Online Test In Process",
					 * Toast.LENGTH_SHORT).show();
					 */

					// Online Test
					/*
					 * setTitle("Notice Board"); setFragment(new
					 * NoticeBoardActivity()); getSupportActionBar().show();
					 */

					Intent i = new Intent(MainActivity.this,
							FragmentRemark.class);
					startActivity(i);

					// getSupportActionBar().show();
				} else if (position == 6) {

					if (NetConnectivity.isOnline(context)) {
						Intent i = new Intent(MainActivity.this,
								FragmentEvents.class);
						startActivity(i);
					} else {
						showDialog();
					}

				} else if (position == 7) {
					// Toast.makeText(context, "Feedback ", Toast.LENGTH_SHORT)
					// .show();
					// Toast.makeText(context, "Ask Question",
					// Toast.LENGTH_SHORT).show();

					// profile
					Intent i = new Intent(MainActivity.this,
							NoticeBoardActivity.class);
					startActivity(i);

					// Toast.makeText(context, "Profile Info",
					// Toast.LENGTH_SHORT).show();

				} else if (position == 8) {
					Intent i = new Intent(MainActivity.this,
							FragmentBusDetails.class);
					startActivity(i);
					/*
					 * Toast.makeText(context, "Profile",
					 * Toast.LENGTH_SHORT).show();
					 */

					// Toast.makeText(context, "Bus Info",
					// Toast.LENGTH_SHORT).show();

				} else if (position == 9) {
					// Toast.makeText(context, "Feedback ", Toast.LENGTH_SHORT)
					// .show();

					// Feedback
					/*
					 * setTitle("CCTV"); setFragment(new CCTVFragment());
					 * getSupportActionBar().show();
					 */
					Intent intent = new Intent(context,
							NotificationActivity.class);
					startActivity(intent);

					// Toast.makeText(context,
					// "Notification Hub",Toast.LENGTH_SHORT).show();

				} else if (position == 10) {

					/*
					 * Intent i = new Intent(context, VideoActivity.class);
					 * startActivity(i);
					 */
					Intent i = new Intent(MainActivity.this,
							FeedbackFragment.class);
					startActivity(i);

					// Toast.makeText(context, "Feedback",
					// Toast.LENGTH_SHORT).show();

				}
				/*
				 * else if(position==11) { setTitle("Examsheet");
				 * setFragment(new ExamsheetFrag());
				 * getSupportActionBar().show(); }
				 */

				else if (position == 11) {
					/*
					 * setTitle("Graph");
					 * 
					 * startActivity(new Intent(context, ReportsDemo.class));
					 */

					Intent i = new Intent(MainActivity.this,
							ContactusActivity.class);
					startActivity(i);

					// Toast.makeText(context, "Contact Us",
					// Toast.LENGTH_SHORT).show();

				} else if (position == 12) {

					Intent i = new Intent(MainActivity.this,
							ParentLinksActivity.class);
					startActivity(i);

				} else if (position == 13) {

					startActivity(new Intent(context, DownloadsActivity.class));

				} else if (position == 14) {

					startActivity(new Intent(context, LeaveActivity.class));

				}

			}

		});

		mTitle = mDrawerTitle = getTitle();

		mMenuTitles = getResources().getStringArray(R.array.menu_array);
		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		mDrawerList = (ListView) findViewById(R.id.left_drawer);
		// mDrawerList.setDividerHeight(2);

		// Set a custom shadow that overlays the main content when the drawer
		// opens
		mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow,
				GravityCompat.START);
		// Prevent the drawer from opening when the user swipes from the left
		mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
		// Set the adapter for the list view
		mDrawerList.setAdapter(new ArrayAdapter<String>(this,
				R.layout.drawer_list_item, mMenuTitles));
		// Set the list's click listener
		mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

		mDrawerToggle = new ActionBarDrawerToggle(this, /* host Activity */
		mDrawerLayout, /* DrawerLayout object */
		R.drawable.ic_launcher, /* nav drawer icon to replace 'Up' caret */
		R.string.drawer_open, /* "open drawer" description */
		R.string.drawer_close /* "close drawer" description */
		) {

			/** Called when a drawer has settled in a completely closed state. */
			public void onDrawerClosed(View view) {
				mDrawerLayout
						.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
				supportInvalidateOptionsMenu();
			}

			/** Called when a drawer has settled in a completely open state. */
			public void onDrawerOpened(View drawerView) {
				mDrawerLayout
						.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
				supportInvalidateOptionsMenu();
			}
		};

		// Set the drawer toggle as the DrawerListener
		mDrawerLayout.setDrawerListener(mDrawerToggle);

		if (getSupportFragmentManager().getBackStackEntryCount() == 0) {
			mDrawerToggle.setDrawerIndicatorEnabled(true);
		} else {
			mDrawerToggle.setDrawerIndicatorEnabled(false);
		}
	}

	@Override
	public void onBackPressed() {
		try {
			if (doubleBackToExitPressedOnce) {
				super.onBackPressed();
				return;
			}

			this.doubleBackToExitPressedOnce = true;
			Toast.makeText(this, "Please click BACK again to exit",
					Toast.LENGTH_SHORT).show();

			new Handler().postDelayed(new Runnable() {

				@Override
				public void run() {
					doubleBackToExitPressedOnce = false;
				}
			}, 2000);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		this.mymenu = menu;
		getMenuInflater().inflate(R.menu.main, menu);
		nametext = menu.findItem(R.id.action_name);

		dbhandler db = new dbhandler(context);
		SQLiteDatabase sd = db.getReadableDatabase();

		String srr = "select * from studinfo where empid="
				+ userDetails.get(sessionmanager.KEY_EMPID);
		Cursor cdept = sd.rawQuery("select * from studinfo where empid="
				+ userDetails.get(sessionmanager.KEY_EMPID), null);
		startManagingCursor(cdept);
		while (cdept.moveToNext()) {

			String name = cdept.getString(2);

			nametext.setTitle(cdept.getString(2));
		}
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// The action bar home/up action should open or close the drawer.
		// ActionBarDrawerToggle will take care of this.

		// Handle action buttons
		switch (item.getItemId()) {
		case android.R.id.home:
			// onBackPressed();

			if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
				Intent i = new Intent(context, MainActivity.class);
				startActivity(i);
			} else {
				boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerList);
				if (drawerOpen) {
					mDrawerLayout.closeDrawer(mDrawerList);
				} else {
					mDrawerLayout.openDrawer(mDrawerList);
				}
			}
			return true;

		case R.id.action_backup:
			backupDB();
			return true;
		case R.id.action_restore:
			importDB();
			return true;
		case R.id.action_sync:
			try {
				new SyncDetailsTask().execute();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return true;
		case R.id.action_logout:
			context.deleteDatabase(dbhandler.db);
			sessionmanager.logoutUser();
			Intent intent = new Intent(context, StartActivity.class);
			startActivity(intent);
			finish();
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	private void showDialog() {
		final Dialog dialog = new Dialog(MainActivity.this);

		dialog.setContentView(R.layout.error_dialog);
		dialog.setTitle("No Internet Connection");

		final ImageView imgError = (ImageView) dialog
				.findViewById(R.id.imgError);

		dialog.show();
	}

	private void showWelcome() {
		final Dialog dialog = new Dialog(MainActivity.this);

		dialog.setContentView(R.layout.thanksmessagedialog);
		dialog.setTitle("Radint Umra");

		final TextView txtWelcome = (TextView) dialog
				.findViewById(R.id.txtTanksMessage);

		txtWelcome.setText("Welcome");
		dialog.show();
	}

	private class DrawerItemClickListener implements
			ListView.OnItemClickListener {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			pos = position;
			selectItem(position);
		}
	}

	private void selectItem(int position) {
		switch (position) {

		case 0:
			// setTitle(""+mMenuTitles[position]);
			// Create a new fragment and show it
			startActivity(new Intent(context, AttendenceFrag.class));
			break;
		case 1:
			setTitle("" + mMenuTitles[position]);
			// Create a new fragment and show it
			// setFragment(new ResultFrag());
			break;
		case 2:
			// setTitle("" + mMenuTitles[position]);
			// Create a new fragment and show it

			break;
		case 3:
			// setTitle("" + mMenuTitles[position]);
			// Create a new fragment and show it
			setFragment(new TestSolution());
			break;
		case 4:
			// setTitle("" + mMenuTitles[position]);
			// Create a new fragment and show it
			setFragment(new HolidayActivity());
			break;
		case 5:
			// setTitle("" + mMenuTitles[position]);
			// setFragment(new SActivityFrag());
			break;
		case 6:
			setTitle("" + mMenuTitles[position]);
			// setFragment(new NoticeBoardActivity());
			break;
		case 7:
			setTitle("" + mMenuTitles[position]);
			// setFragment(new LinksActivity());
			break;
		case 8:
			// setTitle("" + mMenuTitles[position]);
			// setFragment(new CCTVFragment());
			break;
		case 9:
			setTitle("" + mMenuTitles[position]);
			// setFragment(new AppoinmentActivity());
			break;
		case 10:
			// setTitle("" + mMenuTitles[position]);
			// Intent i = new Intent(context, VideoActivity.class);
			// startActivity(i);
			// finish();
			break;
		case 11:

			setTitle("" + mMenuTitles[position]);
			// setFragment(new ExamsheetFrag());
			break;
		case 12:
			context.deleteDatabase("studdb.db");
			Intent intent = new Intent(context, StartActivity.class);
			startActivity(intent);
			finish();
			break;

		case 13:
			context.deleteDatabase("studdb.db");
			Intent inten = new Intent(context, StartActivity.class);
			startActivity(inten);
			finish();
			break;

		}

		mDrawerToggle.setDrawerIndicatorEnabled(false);

		// Highlight the selected item, update the title, and close the drawer
		mDrawerList.setItemChecked(position, true);
		// setTitle(mPlanetTitles[position]);
		mDrawerLayout.closeDrawer(mDrawerList);
	}

	private void setFragment(Fragment fragment) {

		FrameLayout fm = (FrameLayout) findViewById(R.id.content_frame);
		fm.setBackgroundColor(Color.BLACK);
		FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
		ft.setCustomAnimations(android.R.anim.slide_in_left,
				android.R.anim.slide_out_right);
		ft.replace(R.id.content_frame, fragment);
		gridArray.clear();

		gridView.setAdapter(new CustomGridViewAdaptor(context,
				android.R.layout.simple_list_item_1, gridArray));
		ft.addToBackStack(null);
		// Commit the transaction
		ft.commit();

	}

	@Override
	public void setTitle(CharSequence title) {
		mTitle = title;
		getSupportActionBar().setTitle(mTitle);
	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		// Sync the toggle state after onRestoreInstanceState has occurred.
		mDrawerToggle.syncState();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		mDrawerToggle.onConfigurationChanged(newConfig);
	}

	public void backupDB() {
		File sd = Environment.getExternalStorageDirectory();
		File data = Environment.getDataDirectory();
		FileChannel source = null;
		FileChannel destination = null;
		String currentDBPath = "/data/" + context.getPackageName()
				+ "/databases/" + dbhandler.db + "";
		String backupDBPath = dbhandler.db;
		File currentDB = new File(data, currentDBPath);
		File backupDB = new File(sd, backupDBPath);
		try {
			source = new FileInputStream(currentDB).getChannel();
			destination = new FileOutputStream(backupDB).getChannel();
			destination.transferFrom(source, 0, source.size());
			source.close();
			destination.close();
			Toast.makeText(MainActivity.this, "DB Exported!", Toast.LENGTH_LONG)
					.show();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void importDB() {
		try {
			File sd = Environment.getExternalStorageDirectory();
			File data = Environment.getDataDirectory();
			if (sd.canWrite()) {
				String currentDBPath = "//data//" + "parentapp.com"
						+ "//databases//" + "studdatas.db";
				String backupDBPath = "studdb.db";
				File backupDB = new File(data, currentDBPath);
				File currentDB = new File(sd, backupDBPath);

				FileChannel src = new FileInputStream(currentDB).getChannel();
				FileChannel dst = new FileOutputStream(backupDB).getChannel();
				dst.transferFrom(src, 0, src.size());
				src.close();
				dst.close();
				Toast.makeText(getApplicationContext(), "Import Successful!",
						Toast.LENGTH_SHORT).show();

			}
		} catch (Exception e) {

			Toast.makeText(getApplicationContext(), "Import Failed!",
					Toast.LENGTH_SHORT).show();

		}
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		if (mTitle.equals("Student Information") || (mTitle.equals("Parent"))) {
		} else
			outState.putString("pos", "" + pos);
	}

	@Override
	public void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);
		// Restore UI state from the savedInstanceState.
		// This bundle has also been passed to onCreate.

		String poss = savedInstanceState.getString("pos");

		if (poss != null) {
			pos = Integer.parseInt(poss);
			selectItem(pos);
		}

	}

	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			// do something on back.
			super.onBackPressed();
			if (getSupportFragmentManager().getBackStackEntryCount() == 0) {
				setTitle(Html.fromHtml("<font color='#FFFFFF'>" + AppTitle
						+ "</font>"));
				// turn on the Navigation Drawer image; this is called in the
				// LowerLevelFragments
				getSupportActionBar().show();

				mDrawerToggle.setDrawerIndicatorEnabled(true);

				Bitmap attendece = BitmapFactory.decodeResource(
						this.getResources(), R.drawable.ic_attendance);
				Bitmap results = BitmapFactory.decodeResource(
						this.getResources(), R.drawable.ic_result);

				Paint circlePaint = new Paint();
				circlePaint.setAntiAlias(true);
				circlePaint.setColor(Color.RED);

				circlePaint.setStyle(Paint.Style.FILL);
				circlePaint.setAlpha(255);
				circlePaint.setStrokeWidth(10);

				Paint textPaint = new Paint();
				textPaint.setStyle(Style.FILL_AND_STROKE);
				textPaint.setColor(Color.WHITE);
				textPaint.setTextSize(12);
				textPaint.setFakeBoldText(true);

				Bitmap examsheet = BitmapFactory.decodeResource(
						this.getResources(), R.drawable.ic_testsolution);
				Bitmap holiday = BitmapFactory.decodeResource(
						this.getResources(), R.drawable.ic_event);
				Bitmap notification = BitmapFactory.decodeResource(
						this.getResources(), R.drawable.ic_notifications);
				Bitmap contactus = BitmapFactory.decodeResource(
						this.getResources(), R.drawable.ic_contactus);
				/*
				 * Bitmap onlinetest =
				 * BitmapFactory.decodeResource(this.getResources(),
				 * R.drawable.onlinetest);
				 */
				Bitmap timetable = BitmapFactory.decodeResource(
						this.getResources(), R.drawable.ic_timetable);
				/*
				 * Bitmap videos =
				 * BitmapFactory.decodeResource(this.getResources(),
				 * R.drawable.videos);
				 */

				Bitmap feedback = BitmapFactory.decodeResource(
						this.getResources(), R.drawable.ic_feedback);

				/*
				 * Bitmap noticeboard =
				 * BitmapFactory.decodeResource(this.getResources(),
				 * R.drawable.noticeboards); Bitmap testsolution =
				 * BitmapFactory.decodeResource(this.getResources(),
				 * R.drawable.testsolution);
				 * 
				 * Bitmap askquestion =
				 * BitmapFactory.decodeResource(this.getResources(),
				 * R.drawable.askquestion);
				 */

				Bitmap profilepic = BitmapFactory.decodeResource(
						this.getResources(), R.drawable.ic_profile);

				Bitmap events = BitmapFactory.decodeResource(
						this.getResources(), R.drawable.ic_event);
				Bitmap noticeboard = BitmapFactory.decodeResource(
						this.getResources(), R.drawable.ic_noticeboard);
				Bitmap homework = BitmapFactory.decodeResource(
						this.getResources(), R.drawable.ic_homework);
				Bitmap socialmedia = BitmapFactory.decodeResource(
						this.getResources(), R.drawable.ic_socialmedia);
				Bitmap rateapp = BitmapFactory.decodeResource(
						this.getResources(), R.drawable.ic_rating);
				Bitmap achievements = BitmapFactory.decodeResource(
						this.getResources(), R.drawable.ic_achievements);
				Bitmap remarks = BitmapFactory.decodeResource(
						this.getResources(), R.drawable.ic_remark);
				Bitmap businfo = BitmapFactory.decodeResource(
						this.getResources(), R.drawable.ic_businfo);
				Bitmap parentlinks = BitmapFactory.decodeResource(
						this.getResources(), R.drawable.ic_achievements);
				Bitmap downloads = BitmapFactory.decodeResource(
						this.getResources(), R.drawable.ic_downloads);
				Bitmap leave = BitmapFactory.decodeResource(
						this.getResources(), R.drawable.ic_leavedetails);

				gridArray.clear();
				gridArray.add(new Item(profilepic, "Profile"));
				gridArray.add(new Item(attendece, "Attendance"));
				gridArray.add(new Item(results, "Result"));
				gridArray.add(new Item(timetable, "Time Table"));
				gridArray.add(new Item(homework, "Homework"));
				gridArray.add(new Item(remarks, "Remarks"));
				gridArray.add(new Item(events, "Academic Calendar"));
				gridArray.add(new Item(noticeboard, "Notice Board"));
				gridArray.add(new Item(businfo, "Bus Info."));
				gridArray.add(new Item(notification, "Notification hub"));
				gridArray.add(new Item(feedback, "Feedback"));
				gridArray.add(new Item(contactus, "Contact Us"));
				// gridArray.add(new Item(rateapp, "Rate this app"));
				gridArray.add(new Item(parentlinks, "Useful Links"));
				gridArray.add(new Item(downloads, "Downloads"));
				gridArray.add(new Item(leave, "Leave Details"));
				customGridAdapter = new CustomGridViewAdaptor(context,
						R.layout.gridrow, gridArray);
				gridView.setAdapter(customGridAdapter);

			}

			return true;
		}

		return super.onKeyDown(keyCode, event);
	}

	private void clearBackStack() {
		FragmentManager manager = getSupportFragmentManager();
		if (manager.getBackStackEntryCount() > 0) {
			FragmentManager.BackStackEntry first = manager
					.getBackStackEntryAt(0);
			manager.popBackStack(first.getId(),
					FragmentManager.POP_BACK_STACK_INCLUSIVE);
		}
	}

	private final BroadcastReceiver mHandleMessageReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {

			String newMessage = intent.getExtras().getString(
					CommonUtilities.EXTRA_MESSAGE);
			// PaymyreviewGSSThis sms Only From satish sad sad asnd nas dns and
			// sand nsa dnsa ndsandnsa dnsa dns nd sand nsa dn sdn sand san dnsa
			// dnsa nd asd
			// String
			// gg=""+userDetails.get(SessionManagement.KEY_GCMACTIVATION);
			// if(!newMessage.equals("") && gg.equals("true"))
			String query;
			if (newMessage.contains("GSS")) {

				// Waking up mobile if it is sleeping
				WakeLocker.acquire(getApplicationContext());
				// Showing received message
				// lblMessage.append(newMessage + "\n");

				// sd.execSQL("insert into NotificationMst values(null,'DNS IT EXPERTS','"+
				// newMessage +"','datetime()','05:10')");
				String cdate = getDateTime();

				sessionmanager.StorePushNotification("", newMessage, cdate);
				// Notification_Mst(id INTEGER PRIMARY KEY AUTOINCREMENT,header
				// TEXT,notification TEXT,ndate text)");
				// dfsd
				try {
					String TotalMessage = userDetails
							.get(SessionManager.KEY_PUSH_DESCR);

					// String TITLE =
					// TotalMessage = "Hello GSS 123";
					String[] splitted = TotalMessage.split("GSS");
					lstMessage.clear();
					for (String str : splitted) {
						lstMessage.add(str);
						// Do what you need with your tokens here.
						// Toast.makeText(context, "splitted : "+str,
						// Toast.LENGTH_SHORT).show();

					}

				} catch (Exception e) {
					System.out.print("Error : " + e.getMessage());
				}

				query = "insert into Notification_Mst values(null,'"
						+ lstMessage.get(0) + "','" + lstMessage.get(1) + "','"
						+ cdate + "')";

				System.out.print("Current Date : " + cdate);

				// sd.execSQL("insert into NotificationMst values(null,'paymyreview.in','"+
				// newMessage +"','"+ cdate +"','05:10')");
				// Toast.makeText(getApplicationContext(), "New Message: " +
				// newMessage, Toast.LENGTH_LONG).show();
				// Releasing wake lock
				WakeLocker.release();

			}

		}
	};

	private String getDateTime() {
		SimpleDateFormat dateFormat = new SimpleDateFormat(
				"MM-dd-yyyy HH:mm:ss", Locale.getDefault());

		Date date = new Date();
		return dateFormat.format(date);
	}

	// App rating

	public static class AppRater {
		private final static String APP_TITLE = "Radiant app";// App Name
		private final static String APP_PNAME = "com.radiantapp";// Package Name

		private final static int DAYS_UNTIL_PROMPT = 3;// Min number of days
		private final static int LAUNCHES_UNTIL_PROMPT = 3;// Min number of
															// launches

		public static void app_launched(Context mContext) {
			SharedPreferences prefs = mContext.getSharedPreferences("apprater",
					0);
			// if (prefs.getBoolean("dontshowagain", false)) { return ; }

			SharedPreferences.Editor editor = prefs.edit();

			// Increment launch counter
			long launch_count = prefs.getLong("launch_count", 0) + 1;
			editor.putLong("launch_count", launch_count);

			// Get date of first launch
			Long date_firstLaunch = prefs.getLong("date_firstlaunch", 0);
			if (date_firstLaunch == 0) {
				date_firstLaunch = System.currentTimeMillis();
				editor.putLong("date_firstlaunch", date_firstLaunch);
			}
			showRateDialog(mContext, editor);
			// Wait at least n days before opening
			if (launch_count >= LAUNCHES_UNTIL_PROMPT) {
				if (System.currentTimeMillis() >= date_firstLaunch
						+ (DAYS_UNTIL_PROMPT * 24 * 60 * 60 * 1000)) {
					showRateDialog(mContext, editor);
				}
			}

			editor.commit();
		}

		public static void showRateDialog(final Context mContext,
				final SharedPreferences.Editor editor) {
			final Dialog dialog = new Dialog(mContext);
			dialog.setTitle("Rate " + APP_TITLE);

			LinearLayout ll = new LinearLayout(mContext);
			ll.setOrientation(LinearLayout.VERTICAL);

			LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
					LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
			// params.setMargins(left, top, right, bottom);
			params.setMargins(8, 8, 8, 8);

			TextView tv = new TextView(mContext);
			tv.setText("If you enjoy using "
					+ APP_TITLE
					+ ", please take a moment to rate it. Thanks for your support!");
			tv.setWidth(240);
			tv.setPadding(4, 0, 4, 10);
			tv.setLayoutParams(params);
			ll.addView(tv);

			Button b1 = new Button(mContext);
			b1.setBackgroundResource(R.color.dark_blue);
			b1.setTextColor(Color.parseColor("#FFFFFF"));
			b1.setText("Rate " + APP_TITLE);

			b1.setLayoutParams(params);
			b1.setOnClickListener(new View.OnClickListener() {
				public void onClick(View v) {
					mContext.startActivity(new Intent(Intent.ACTION_VIEW, Uri
							.parse("market://details?id=" + APP_PNAME)));
					dialog.dismiss();
				}
			});
			ll.addView(b1);

			Button b2 = new Button(mContext);
			b2.setText("Remind me later");
			b2.setBackgroundResource(R.color.dark_blue);
			b2.setTextColor(Color.parseColor("#FFFFFF"));
			b2.setLayoutParams(params);
			b2.setOnClickListener(new View.OnClickListener() {
				public void onClick(View v) {

					dialog.dismiss();
				}
			});
			ll.addView(b2);

			Button b3 = new Button(mContext);
			b3.setBackgroundResource(R.color.dark_blue);
			b3.setTextColor(Color.parseColor("#FFFFFF"));
			b3.setText("No, thanks");
			b3.setLayoutParams(params);
			b3.setOnClickListener(new View.OnClickListener() {
				public void onClick(View v) {
					if (editor != null) {
						editor.putBoolean("dontshowagain", true);
						editor.commit();
					}
					dialog.dismiss();
				}
			});
			ll.addView(b3);

			dialog.setContentView(ll);
			dialog.show();
		}
	}

	// Complete app rating

	private class SyncDetailsTask extends AsyncTask<String, Void, String> {

		private String url1;
		final ProgressDialog pDialog1 = new ProgressDialog(
				getApplicationContext());
		private JSONArray jarrayholiday;
		private String url;
		private JSONObject json6;
		private String urltestResult;
		private String urlsubject;
		private String urlfaculty;
		private String depturl;
		private String urlNotice;
		private String urlBOD;
		private int m;
		private int min;
		private PendingIntent pendingIntent;

		@Override
		protected void onPreExecute() {
			try {

				gridView.setVisibility(View.GONE);
				prgShow.setVisibility(View.VISIBLE);
				lblSyncText.setVisibility(View.VISIBLE);

				super.onPreExecute();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		@Override
		protected String doInBackground(String... urls) {
			String response = "";

			JSONParser jParser = new JSONParser();
			String qry = "Select * from DataUrl";
			dbhandler db = new dbhandler(context);
			SQLiteDatabase sd = db.getReadableDatabase();
			Cursor cdata = sd.rawQuery(qry, null);

			while (cdata.moveToNext()) {
				url1 = cdata.getString(0);
			}

			Cursor cdept = sd.rawQuery("select * from studinfo", null);

			while (cdept.moveToNext()) {
				// deptid=cdept.getString(0);
				// empid=cdept.getString(1);
			}
			Cursor cmax = sd.rawQuery("select max(date) from studattendence",
					null);
			while (cmax.moveToNext()) {
				date = cmax.getString(0);
			}

			// getting JSON string from URL
			try {
				urlfaculty = "http://panel.dnsitexperts.com/ServiceP.asmx/GetFacultiesData?type=Faculty&clientid="
						+ AllKeys.CLIENT_ID
						+ "&branch="
						+ userDetails.get(SessionManager.KEY_BRANCHID);
				ServiceHandler sh = new ServiceHandler();
				String str_stud_faculty = sh.makeServiceCall(urlfaculty,
						ServiceHandler.GET);
				// JSONObject json1 = jParser.getJSONFromUrl(url);

				if (str_stud_faculty != null && !str_stud_faculty.equals("")) {

					str_stud_faculty = "{" + '"' + "Faculty" + '"' + ":"
							+ str_stud_faculty + "}";
					try {
						JSONObject jsonContact = new JSONObject(
								str_stud_faculty);
						if (jsonContact != null) {
							try {
								Log.d("Faculty Json : ", jsonContact.toString());
								// Getting Array of Contacts
								jarrayholiday = jsonContact
										.getJSONArray("Faculty");

								// looping through All Contacts
								sd.delete("staffinfo", null, null);
								for (int i = 0; i < jarrayholiday.length(); i++) {
									JSONObject c = jarrayholiday
											.getJSONObject(i);

									sd.execSQL("INSERT INTO staffinfo VALUES('"
											+ c.getString("FacultyId") + "','"
											+ c.getString("FacultyName")
											+ "') ");

								}

							} catch (JSONException e) {
								e.printStackTrace();
								Log.d("Faculty Json Error : ", e.getMessage());
							}
						}
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			jParser = new JSONParser();

			// getting JSON string from URL
			urlsubject = "http://panel.dnsitexperts.com/ServiceP.asmx/GetSubjectsData?type=Subject&clientid="
					+ AllKeys.CLIENT_ID
					+ "&branch="
					+ userDetails.get(SessionManager.KEY_BRANCHID);
			ServiceHandler shSubject = new ServiceHandler();
			String str_stud_Subject = shSubject.makeServiceCall(urlsubject,
					ServiceHandler.GET);
			if (str_stud_Subject != null && !str_stud_Subject.equals("")) {

				str_stud_Subject = "{" + '"' + "SubjectMst" + '"' + ":"
						+ str_stud_Subject + "}";

				// json2 = jParser.getJSONFromUrl(url);
				try {
					JSONObject json = new JSONObject(str_stud_Subject);
					if (json != null) {
						try {
							Log.d("subject json", "" + json.toString());
							// Getting Array of Contacts
							jarrayholiday = json.getJSONArray("SubjectMst");

							// looping through All Contacts
							sd.delete("submaster", null, null);
							for (int i = 0; i < jarrayholiday.length(); i++) {
								JSONObject c = jarrayholiday.getJSONObject(i);
								sd.execSQL("INSERT INTO submaster VALUES('"
										+ c.getString("code") + "','"
										+ c.getString("name") + "') ");
							}

						} catch (JSONException e) {
							e.printStackTrace();
							Log.d("Subject json error", e.getMessage());
						}
					}
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			depturl = "http://panel.dnsitexperts.com/ServiceP.asmx/GetStandardsData?type=standard&clientid="
					+ AllKeys.CLIENT_ID
					+ "&branch="
					+ userDetails.get(SessionManager.KEY_BRANCHID);
			// JSONObject jsonDept = jparser
			// .getJSONFromUrl(depturl);

			ServiceHandler sh1 = new ServiceHandler();
			String str_stud_remark = sh1.makeServiceCall(depturl,
					ServiceHandler.GET);
			if (str_stud_remark != null && !str_stud_remark.equals("")) {

				str_stud_remark = "{" + '"' + "Dept" + '"' + ":"
						+ str_stud_remark + "}";
				try {
					JSONObject jsonDept = new JSONObject(str_stud_remark);
					if (jsonDept != null) {
						try {
							JSONArray jarray = jsonDept.getJSONArray("Dept");

							for (int i = 0; i < jarray.length(); i++) {
								JSONObject jobj = jarray.getJSONObject(i);

								String did = jobj.getString("StdId");
								String dname = jobj.getString("StdName");

								sd.execSQL("insert into DeptMaster values('"
										+ did + "','" + dname + "')");
							}
						} catch (SQLException e) {
							// TODO Auto-generated catch
							// block
							e.printStackTrace();
						} catch (JSONException e) {
							// TODO Auto-generated catch
							// block
							e.printStackTrace();
						}

					}
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			jParser = new JSONParser();

			// getting JSON string from URL
			String urlAttendance = "http://panel.dnsitexperts.com/ServiceP.asmx/GetAttendanceData?type=A&EmpId="
					+ empid
					+ "&DeptId="
					+ deptid
					+ "&clientid="
					+ AllKeys.CLIENT_ID
					+ "&branch="
					+ userDetails.get(SessionManager.KEY_BRANCHID);

			ServiceHandler shAtt = new ServiceHandler();
			String str_stud_att = shAtt.makeServiceCall(urlAttendance,
					ServiceHandler.GET);

			if (str_stud_att != null && !str_stud_att.equals("")) {

				str_stud_att = "{" + '"' + "TransDet" + '"' + ":"
						+ str_stud_att + "}";
				try {
					JSONObject json = new JSONObject(str_stud_att);
					if (json != null) {
						try {
							// Getting Array of Contacts
							jarrayholiday = json.getJSONArray("TransDet");

							// looping through All Contacts
							sd.delete("studattendence", null, null);
							for (int i = 0; i < jarrayholiday.length(); i++) {
								JSONObject c = jarrayholiday.getJSONObject(i);

								sd.execSQL("INSERT INTO studattendence VALUES('"
										+ c.getString("AttendanceDate")
										+ "','"
										+ c.getString("Flag") + "') ");
							}

						} catch (JSONException e) {
							e.printStackTrace();
						}
					}
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
			jParser = new JSONParser();
			urlNotice = "http://panel.dnsitexperts.com/serviceP.asmx/GetNoticeBoardsData?type=NoticeBoard&classid="
					+ deptid
					+ "&clientid="
					+ AllKeys.CLIENT_ID
					+ "&branch="
					+ userDetails.get(SessionManager.KEY_BRANCHID);
			// getting JSON string from URL
			// url = url1 + "json_data.aspx?type=notice";
			ServiceHandler shNotice = new ServiceHandler();
			String str_stud_Notice = shNotice.makeServiceCall(urlNotice,
					ServiceHandler.GET);

			if (str_stud_Notice != null && !str_stud_Notice.equals("")) {

				str_stud_Notice = "{" + '"' + "NoticeBoard" + '"' + ":"
						+ str_stud_Notice + "}";
				// json6 = jParser.getJSONFromUrl(url);
				try {
					JSONObject jsonn = new JSONObject(str_stud_Notice);
					if (jsonn != null) {
						try {
							// Getting Array of Contacts
							jarrayholiday = jsonn.getJSONArray("NoticeBoard");

							// looping through All Contacts
							sd.delete("noticeboard", null, null);
							for (int i = 0; i < jarrayholiday.length(); i++) {
								JSONObject c = jarrayholiday.getJSONObject(i);
								// CharSequence
								// content=(Html.fromHtml(c.getString("LongDescription")));
								sd.execSQL("INSERT INTO noticeboard VALUES('"
										+ c.getString("Id") + "','"
										+ c.getString("ShortDescription")
										+ "','"
										+ c.getString("LongDescription")
										+ "','" + c.getString("Date") + "','"
										+ c.getString("IsShown") + "') ");

							}

						} catch (JSONException e) {
							e.printStackTrace();
						}

					}
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			jParser = new JSONParser();

			// getting JSON string from URL

			// JSONObject json = jParser.getJSONFromUrl(url);

			jParser = new JSONParser();

			// getting JSON string from URL
			/*
			 * url = url1 + "json_data.aspx?type=Video"; JSONObject jsonvideo =
			 * jParser.getJSONFromUrl(url); if (jsonvideo != null) {
			 * 
			 * try { // Getting Array of Contacts jarrayholiday =
			 * jsonvideo.getJSONArray("Video");
			 * 
			 * // looping through All Contacts sd.delete("Video", null, null);
			 * for (int i = 0; i < jarrayholiday.length(); i++) { JSONObject c =
			 * jarrayholiday.getJSONObject(i);
			 * 
			 * sd.execSQL("INSERT INTO Video VALUES('" + c.getString("VideoId")
			 * + "','" + c.getString("VideoName") + "','" +
			 * c.getString("VideoUrl") + "','" + c.getString("DeptId") + "','" +
			 * c.getString("SubId") + "','" + c.getString("Chapter") + "') ");
			 * 
			 * }
			 * 
			 * } catch (JSONException e) {
			 * 
			 * e.printStackTrace(); }
			 * 
			 * }
			 */

			jParser = new JSONParser();

			// getting JSON string from URL
			urlBOD = "http://panel.dnsitexperts.com/ServiceP.asmx/GetStudentDetails?type=BTD&deptid="
					+ deptid
					+ "&empid="
					+ empid
					+ "&clientid="
					+ AllKeys.CLIENT_ID
					+ "&branch="
					+ userDetails.get(SessionManager.KEY_BRANCHID);
			// getting JSON string from URL
			// url = url1 + "json_data.aspx?type=BTD&DeptId=" + deptid +
			// "&EmpId="+ empid;
			ServiceHandler shBOD = new ServiceHandler();
			String str_stud_BOD = shBOD.makeServiceCall(urlBOD,
					ServiceHandler.GET);

			// JSONObject jsonbday = jParser.getJSONFromUrl(url);
			if (str_stud_BOD != null && !str_stud_BOD.equals("")) {

				str_stud_BOD = "{" + '"' + "StudentBirthday" + '"' + ":"
						+ str_stud_BOD + "}";
				try {
					JSONObject jsonbday = new JSONObject(str_stud_BOD);
					if (jsonbday != null) {

						try {
							// Getting Array of Contacts
							jarrayholiday = jsonbday
									.getJSONArray("StudentBirthday");

							// looping through All Contacts
							sd.delete("birthday", null, null);

							for (int i = 0; i < jarrayholiday.length(); i++) {
								JSONObject c = jarrayholiday.getJSONObject(i);

								sd.execSQL("INSERT INTO birthday VALUES('"
										+ c.getString("EmpId") + "','"
										+ c.getString("Name") + "','"
										+ c.getString("DOB") + "') ");
								String dt = c.getString("DOB");
								if (!c.getString("DOB").equals("")) {
									String[] sdt = dt.split("-");
									int mon = Integer.parseInt(sdt[1]);
									int day = Integer.parseInt(sdt[2]);
									int eid = Integer.parseInt(c
											.getString("EmpId"));
									if (mon >= 1)
										m = mon - 1;
									Calendar calendar = Calendar.getInstance();

									calendar.set(Calendar.MONTH, m);
									calendar.set(Calendar.YEAR,
											calendar.get(Calendar.YEAR));
									calendar.set(Calendar.DAY_OF_MONTH, day);

									calendar.set(Calendar.HOUR_OF_DAY, 8);
									calendar.set(Calendar.MINUTE, 30);
									calendar.set(Calendar.SECOND, 0);
									calendar.set(Calendar.AM_PM, Calendar.AM);

									Intent myIntent = new Intent(context,
											MyBroadcastReceiver.class);
									// Birthday Notification
									// myIntent.putExtra("name",
									// "Happy Birthday "+c.getString("Name"));
									// myIntent.putExtra("eid", ""+eid);
									// pendingIntent =
									// PendingIntent.getBroadcast(context, eid,
									// myIntent,PendingIntent.FLAG_UPDATE_CURRENT);

									AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
									alarmManager.set(AlarmManager.RTC,
											calendar.getTimeInMillis(),
											pendingIntent);

								}

							}
						} catch (JSONException e) {

							e.printStackTrace();
						}

					}
				} catch (NumberFormatException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			jParser = new JSONParser();

			// getting JSON string from URL
			url = "http://panel.dnsitexperts.com/ServiceP.asmx/GetTestMasterData?type=TD&DeptId="
					+ deptid
					+ "&Trsrno=0&clientid="
					+ AllKeys.CLIENT_ID
					+ "&branch=" + userDetails.get(SessionManager.KEY_BRANCHID);
			ServiceHandler shTD = new ServiceHandler();
			String str_stud_TD = shTD.makeServiceCall(url, ServiceHandler.GET);
			if (str_stud_TD != null && !str_stud_TD.equals("")) {

				str_stud_TD = "{" + '"' + "Test" + '"' + ":" + str_stud_TD
						+ "}";
				try {
					JSONObject jsontest = new JSONObject(str_stud_TD);
					// JSONObject jsontest = jParser.getJSONFromUrl(url);
					if (jsontest != null) {

						try {
							// Getting Array of Contacts
							jarrayholiday = jsontest.getJSONArray("Test");

							// looping through All Contacts
							sd.delete("testmaster", null, null);
							for (int i = 0; i < jarrayholiday.length(); i++) {
								JSONObject c = jarrayholiday.getJSONObject(i);
								qry = "INSERT INTO testmaster VALUES('"
										+ c.getString("TestId") + "','"
										+ c.getString("TestDate") + "','"
										+ c.getString("TestName") + "','"
										+ c.getString("TotMks") + "','"
										+ c.getString("PassMks") + "','"
										+ c.getString("DeptId") + "','"
										+ c.getString("SubId") + "') ";
								sd.execSQL(qry);

							}

						} catch (JSONException e) {

							e.printStackTrace();
						}
					}
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			jParser = new JSONParser();

			// getting JSON string from URL
			urltestResult = "http://panel.dnsitexperts.com/serviceP.asmx/GetTestDetailsData?type=TDD&DeptId="
					+ deptid
					+ "&EmpId="
					+ empid
					+ "&Trsrno=0&clientid="
					+ AllKeys.CLIENT_ID
					+ "&branch="
					+ userDetails.get(SessionManager.KEY_BRANCHID);
			// url = url1 + "json_data.aspx?type=TDD&DeptId=" + deptid +
			// "&EmpId="
			// + empid;

			ServiceHandler shTDD = new ServiceHandler();
			String str_stud_TDD = shTDD.makeServiceCall(urltestResult,
					ServiceHandler.GET);
			if (str_stud_TDD != null && !str_stud_TDD.equals("")) {
				str_stud_TDD = "{" + '"' + "TestResult" + '"' + ":"
						+ str_stud_TDD + "}";
				try {
					try {
						JSONObject json = new JSONObject(str_stud_TDD);
						if (json != null) {
							try {
								// Getting Array of Contacts
								jarrayholiday = json.getJSONArray("TestResult");

								// looping through All Contacts
								sd.delete("testdetail", null, null);
								for (int i = 0; i < jarrayholiday.length(); i++) {
									JSONObject c = jarrayholiday
											.getJSONObject(i);

									sd.execSQL("INSERT INTO testdetail VALUES('"
											+ c.getString("TestId")
											+ "','"
											+ c.getString("StudId")
											+ "','"
											+ c.getString("StudName")
											+ "','"
											+ c.getString("SubId")
											+ "','"
											+ c.getString("Marks")
											+ "','"
											+ c.getString("Percentage")
											+ "','"
											+ c.getString("HighestMks")
											+ "','"
											+ c.getString("DeptId") + "') ");

								}

							} catch (JSONException e) {

								e.printStackTrace();
							}
						}
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			JSONParser jParser1 = new JSONParser();

			// getting JSON string from URL
			/*
			 * url = url1 + "json_data.aspx?type=EmpAppointment&AID=0&DeptId=" +
			 * deptid + "&EmpId=" + empid; jsonnotes =
			 * jParser1.getJSONFromUrl(url);
			 * 
			 * if (jsonnotes != null) { try { // Getting Array of Contacts
			 * jarrayholiday = jsonnotes.getJSONArray("Appointment");
			 * 
			 * // looping through All Contacts sd.delete("appoinment", null,
			 * null); for (int i = 0; i < jarrayholiday.length(); i++) {
			 * JSONObject c = jarrayholiday.getJSONObject(i);
			 * 
			 * sd.execSQL("INSERT INTO appoinment VALUES('" +
			 * c.getString("AppointmentId") + "','" + c.getString("Date") +
			 * "','" + c.getString("Time") + "','" + c.getString("Reason") +
			 * "','" + c.getString("DeptId") + "','" + c.getString("EmpId") +
			 * "','" + c.getString("AppFlag") + "') "); }
			 * 
			 * } catch (JSONException e) { e.printStackTrace(); } }
			 */

			jParser = new JSONParser();

			// getting JSON string from URL
			/*
			 * url = url1 + "json_data.aspx?type=MobileSite"; JSONObject
			 * jsonmobile = jParser.getJSONFromUrl(url); if (jsonmobile == null)
			 * {
			 * 
			 * } else {
			 * 
			 * try { // Getting Array of Contacts jarrayholiday =
			 * jsonmobile.getJSONArray("MobilePage");
			 * 
			 * // looping through All Contacts sd.delete("MobileTitle", null,
			 * null); for (int i = 0; i < jarrayholiday.length(); i++) {
			 * JSONObject c = jarrayholiday.getJSONObject(i); CharSequence
			 * content = (Html.fromHtml(c .getString("Content")));
			 * sd.execSQL("INSERT INTO MobileTitle VALUES('" +
			 * c.getString("MobilePageId") + "','" + c.getString("Title") +
			 * "','" + content.toString() + "') ");
			 * 
			 * }
			 * 
			 * } catch (JSONException e) {
			 * 
			 * e.printStackTrace(); } }
			 */
			jParser = new JSONParser();

			// getting JSON string from URL
			/*
			 * url = url1 + "json_data.aspx?DeptId=" + deptid +
			 * "&type=TimeTable"; json6 = jParser.getJSONFromUrl(url);
			 * 
			 * if (json6 != null) { try { // Getting Array of Contacts
			 * jarrayholiday = json6.getJSONArray("TimeTable");
			 * 
			 * // looping through All Contacts sd.delete("time_tb_mst", null,
			 * null); for (int i = 0; i < jarrayholiday.length(); i++) {
			 * JSONObject c = jarrayholiday.getJSONObject(i);
			 * 
			 * sd.execSQL("IN" + "SERT INTO time_tb_mst VALUES('" +
			 * c.getString("DeptId") + "','" + c.getString("Time") + "','" +
			 * c.getString("Day") + "','" + c.getString("Subject") + "') "); } }
			 * catch (JSONException e) { e.printStackTrace(); } }
			 */
			return response;
		}

		@Override
		protected void onPostExecute(String result) {

			prgShow.setVisibility(View.GONE);
			lblSyncText.setVisibility(View.GONE);
			gridView.setVisibility(View.VISIBLE);

		}
	}

}
